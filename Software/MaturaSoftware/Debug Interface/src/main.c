/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <avr/io.h>
#include <interrupt.h>

volatile uint8_t stuff;

int main (void)
{
	//sysclk_init();

	//board_init();
	
	//ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTE, 1), IOPORT_DIR_OUTPUT);
	//ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTE, 0), IOPORT_DIR_OUTPUT);

	//sysclk_enable_module(SYSCLK_PORT_E, SYSCLK_TWI);

	pmic_init();

	/*twi_options_t opts = {
		.speed = 5000,
		.speed_reg = TWI_BAUD(sysclk_get_per_hz(), 5000),
		.chip = 1
	};

	twi_master_init(&TWIE, &opts);

	uint8_t buffer[] = {0xA4, 0x55, 0xD7};

	twi_package_t pkg = {
		.addr_length = 0,
		.chip = 1,
		.buffer = &buffer,
		.length = 3,
		.no_wait = false
	};

	twi_master_enable(&TWIE);

	while(1) {
		twi_master_write(&TWIE, &pkg);
	}*/

	TWIE.MASTER.BAUD = TWI_BAUD(32000000, 8000);
	TWIE.MASTER.CTRLB = TWI_MASTER_SMEN_bm;
	TWIE.MASTER.CTRLA = TWI_MASTER_INTLVL_HI_gc | TWI_MASTER_RIEN_bm | TWI_MASTER_WIEN_bm | TWI_MASTER_ENABLE_bm;
	
	cpu_irq_enable();

	TWIE.MASTER.ADDR = 0x03;

	while(1) {
		/*ioport_set_pin_level(IOPORT_CREATE_PIN(PORTE, 1), 0);
		volatile uint32_t cnt = 10;
		while(cnt) {
			cnt--;
		}
		ioport_set_pin_level(IOPORT_CREATE_PIN(PORTE, 1), 1);
		cnt = 10;
		while(cnt) {
			cnt--;
		}*/

	}
}
