/*
 * endpoint.h
 *
 * Created: 02.08.2016 15:04:02
 *  Author: Lorenzo Rai
 */ 


#ifndef ENDPOINT_H_
#define ENDPOINT_H_

#define EP_RTC_HZ		32768
#define EP_TIME_UNIT	(60 * 60)

void ep_init(void);

void ep_start(void);

void ep_update(uint32_t time);

void ep_set_target(float target);

float ep_get_target(void);

void ep_set_stored(float stored);

float ep_get_stored(void);

#endif /* ENDPOINT_H_ */