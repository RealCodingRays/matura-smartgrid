/*
 * regulator.c
 *
 * Created: 13.07.2016 12:26:43
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <util/atomic.h>
 #include <float.h>
 #include "regulator.h"
 #include "config_manager.h"
 #include "sensors.h"
 #include "timer.h"

 volatile float reg_max_power = 0;

 volatile uint8_t reg_should_update = 0;

 volatile uint8_t stuff;

 void reg_init() {
	tc45_enable(&REG_TC);
	tc45_set_wgm(&REG_TC, TC45_WG_SS);
	tc45_enable_cc_channels(&REG_TC, TC45_CCACOMP);

	ioport_set_pin_dir(REG_LED_RED, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(REG_LED_GREEN, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(REG_CTRL, IOPORT_DIR_OUTPUT);

	reg_reload_config();
 }

 void reg_reload_config() {
	tc45_write_period(&REG_TC, CFG_REGULATOR_TOP);
	tc45_write_cc(&REG_TC, REG_CC, CFG_REGULATOR_MIN);
 }

 void reg_set_target(float target) {
	reg_max_power = target;
 }

 float reg_get_target() {
	 return reg_max_power;
 }

 void reg_start() {
	tc45_write_cc(&REG_TC, REG_CC, CFG_REGULATOR_MIN);
	ioport_set_pin_dir(REG_CTRL, IOPORT_DIR_OUTPUT);
	ioport_set_pin_level(REG_CTRL, 0);
	tc45_write_clock_source(&REG_TC, TC45_CLKSEL_DIV1_gc);
	ioport_set_pin_level(REG_LED_GREEN, 1);
	ioport_set_pin_level(REG_LED_RED, 0);
 }

 void reg_stop() {
	ioport_set_pin_level(REG_CTRL, 0);
	ioport_set_pin_dir(REG_CTRL, IOPORT_DIR_INPUT);
	tc45_write_clock_source(&REG_TC, TC45_CLKSEL_OFF_gc);
	tc45_write_count(&REG_TC, 0);
	ioport_set_pin_level(REG_LED_GREEN, 0);
	ioport_set_pin_level(REG_LED_RED, 0);
 }

 void reg_new_data() {
	reg_should_update = 1;
 }
 
 void reg_update() {
	 float max_volts = 0;
	 if(timer_using_profile()) {
		max_volts = sen_last_data->curr > 0 ? (timer_get_current_target() / sen_last_data->curr) : (timer_get_current_target() > 0 ? FLT_MAX : 0);
	 } else {
		max_volts = sen_last_data->curr > 0 ? (reg_max_power / sen_last_data->curr) : FLT_MAX;
	 }

	 if((!cfg_is_disabled(CFG_REGULATOR)) && (max_volts > REG_VOLTAGE_MIN)) {
		 if(reg_should_update) {
			 reg_should_update = 0;

			 float voltage_target = CFG_VOLTAGE_TARGET + 0.4;

			 if(sen_last_data->vreg > max_volts + 0.4) {
				voltage_target = max_volts;
				ioport_set_pin_level(REG_LED_RED, 1);
				ioport_set_pin_level(REG_LED_GREEN, 0);
			 }

			 float delta = voltage_target - sen_last_data->vreg;
			 float dnorm = delta / ((float) SEN_VDIV_FACTOR);
			 float change = dnorm * ((float) CFG_REGULATOR_TOP);

			 int16_t old = (int16_t) tc45_read_cc(&REG_TC, REG_CC);
			 uint16_t new;

			 if((change > 0) && ((CFG_REGULATOR_MAX - change) < old)) {
				 new = CFG_REGULATOR_MAX;
				 ioport_set_pin_level(REG_LED_RED, 1);
				 ioport_set_pin_level(REG_LED_GREEN, 0);
			 } else if((change < 0) && ((CFG_REGULATOR_MIN - change) > old)) {
				 new = CFG_REGULATOR_MIN;
				 ioport_set_pin_level(REG_LED_GREEN, 1);
				 ioport_set_pin_level(REG_LED_RED, 1);
			 } else {
				 new = (uint16_t) (old + change);
			 	 ioport_set_pin_level(REG_LED_GREEN, 1);
			 	 ioport_set_pin_level(REG_LED_RED, 0);
			 }
			 tc45_write_cc(&REG_TC, REG_CC, new);
		 }
	 } else {
		 tc45_write_cc(&REG_TC, REG_CC, 0);
		 ioport_set_pin_level(REG_LED_GREEN, 0);
		 ioport_set_pin_level(REG_LED_RED, 0);
	 }
 }