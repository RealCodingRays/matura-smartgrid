/*
 * endpoint.c
 *
 * Created: 02.08.2016 15:06:04
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <common/tba.h>
 #include <util/atomic.h>
 #include "endpoint.h"
 #include "sensors.h"
 #include "regulator.h"
 #include "load.h"
 #include "config_manager.h"

 volatile float ep_target;
 volatile float ep_stored;
 volatile uint8_t ep_should_update;

 volatile float ep_last_curr;

 void ep_init() {
	rtc_init();
	rtc_set_callback(ep_update);
	rtc_set_alarm(100);
	ep_target = 0;
	ep_stored = 0;
	ep_should_update = 0;
	ep_last_curr = 0;
 }

 void ep_start() {
	rtc_set_time(0);
	reg_set_target(0);
	load_set_power(0);
 }

 void ep_update(uint32_t time) {
	if(CFG_TYPE == TBA_EP_TYPE_BATTERY) {

		float time_unit = ((1.0 / ((float) EP_RTC_HZ)) * ((float) time)) / EP_TIME_UNIT;

		float power = 0;
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			float curr = (sen_last_data->curr - ep_last_curr) / 2;
			if(curr < SEN_CURR_RES && curr > -SEN_CURR_RES) {
				curr = 0;
			}
			ep_last_curr = sen_last_data->curr;
			power = curr * sen_last_data->vout * time_unit;
		}
		ep_stored -= power;
	}
	rtc_set_time(0);
 }

 void ep_set_target(float target) {
	reg_set_target(target);
	load_set_power(target);
	ep_target = target;
 }

 float ep_get_target() {
	return ep_target;
 }

 void ep_set_stored(float stored) {
	ep_stored = 0;
 }

 float ep_get_stored() {
	return ep_stored;
 }