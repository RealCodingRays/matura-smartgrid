/*
 * comm.h
 *
 * Created: 29.07.2016 17:35:26
 *  Author: Lorenzo Rai
 */ 


#ifndef COMM_H_
#define COMM_H_

#include <inttypes.h>

#define BOARD_USART					USARTD0
#define BOARD_USART_XCK				IOPORT_CREATE_PIN(PORTD, 1)

#define COMM_TX_IDLE				0x00
#define COMM_TX_SENDING				0x01

#define COMM_RX_NORMAL				0x00
#define COMM_RX_BUSERR_bm			0x01
#define COMM_RX_DISCARD_bm			0x02

#define COMM_STATE_UNINITIALIZED	0x00
#define COMM_STATE_AWAIT_READ		0x01
#define COMM_STATE_PRE_INIT			0x02
#define COMM_STATE_INIT				0x03
#define COMM_STATE_TIMEOUT			0x04
#define COMM_STATE_SEND_DATA		0x05
#define COMM_STATE_AWAIT_ADDRESS	0x06
#define COMM_STATE_INITIALIZED		0x07

#define COMM_INIT_TIMEOUT			10

void comm_init(void);

void comm_start(void);

void comm_update(void);

void comm_send_cmd(uint8_t cmd);

void comm_send_data(uint8_t cmd, tba_data_t *data);

void comm_handle_protocol_error(void);

void comm_handle_init(void);

void comm_send_device(void);

void comm_send_state(void);

void comm_set_address(void);

void comm_handle_set_target(void);

void comm_rtc_callback(uint32_t time);


void comm_handle_rx(void);

void comm_handle_tx(void);

#endif /* COMM_H_ */