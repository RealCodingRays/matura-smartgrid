/*
 * timer.c
 *
 * Created: 16.07.2016 13:16:54
 *  Author: Lorenzo Rai
 */ 

 #include <util/atomic.h>
 #include <math.h>
 #include "timer.h"
 #include "config_manager.h"

 volatile float *timer_profile;
 volatile size_t timer_size;

 void timer_init() {
	tc45_enable(&TIMER_TC_CLK);
	tc45_set_wgm(&TIMER_TC_CLK, TC45_WG_NORMAL);

	EVSYS.CH7MUX = EVSYS_CHMUX_TCC4_OVF_gc;

	tc45_enable(&TIMER_TC_TIME);
	tc45_set_wgm(&TIMER_TC_TIME, TC45_WG_NORMAL);
	tc45_write_clock_source(&TIMER_TC_TIME, TC45_CLKSEL_EVCH7_gc);
 }

 void timer_start() {
	tc45_write_clock_source(&TIMER_TC_CLK, TIMER_DIV);
 }

 void timer_stop() {
	tc45_write_clock_source(&TIMER_TC_CLK, TC45_CLKSEL_OFF_gc);
 }

 void timer_reset() {
	tc45_write_count(&TIMER_TC_CLK, 0);
	tc45_write_count(&TIMER_TC_TIME, 0);
 }

 void timer_set_profile(float *data, size_t size) {
	 timer_size = size;
	 timer_profile = (size > 0 ? data : NULL);
 }

 float timer_get_current_target() {
	if(timer_profile) {
		size_t index = ((size_t) tc45_read_count(&TIMER_TC_TIME)) % timer_size;
	
		float y1 = timer_profile[index];
		float y2 = timer_profile[(index + 1 < timer_size ? index : 0)];

		float x = ((float) tc45_read_count(&TIMER_TC_CLK)) / ((float) TIMER_INTERVAL);

		//Interpolation methods source: http://paulbourke.net/miscellaneous/interpolation/ by Paul Bourke
		//Linear Interpolation
		return (y1*(1-x)+y2*x);

		//Cosine Interpolation
		float x2 = (1-cos(x*M_PI))/2;
		return(y1*(1-x2)+y2*x2);
	} else {
		return 0;
	}
 }

 uint8_t timer_using_profile() {
	return timer_profile != NULL;
 }