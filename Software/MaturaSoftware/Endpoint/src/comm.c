/*
 * comm.c
 *
 * Created: 29.07.2016 17:37:19
 *  Author: Lorenzo Rai
 */ 

 #include <common/ring_buffer.h>
 #include <common/tba.h>
 #include <asf.h>
 #include <stdlib.h>
 #include <avr/interrupt.h>
 #include <util/atomic.h>
 #include "comm.h"
 #include "sensors.h"
 #include "config_manager.h"
 #include "endpoint.h"

 ring_buffer_t comm_read_buffer;
 ring_buffer_t comm_write_buffer;

 tba_packet_t *comm_temp;

 volatile uint8_t comm_address;
 volatile uint8_t comm_state;
 volatile uint8_t comm_rx_state;
 volatile uint8_t comm_tx_state;
 volatile uint16_t comm_cnt = 0;

 void comm_init() {
	usart_serial_options_t opts = {
		.baudrate = 4800,
		.charlength = USART_CHSIZE_9BIT_gc,
		.paritytype = USART_PMODE_ODD_gc,
		.stopbits = true
	};

	usart_serial_init(&BOARD_USART, &opts);
	BOARD_USART.CTRLB |= USART_MPCM_bm;

	rb_init(&comm_read_buffer, 128);
	rb_init(&comm_write_buffer, 128);

	comm_address = TBA_DEFAULT_ADDRESS;
	comm_state = COMM_STATE_UNINITIALIZED;
	comm_rx_state = 0;
	comm_tx_state = COMM_TX_IDLE;

	ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 3), IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 1), IOPORT_DIR_OUTPUT);
 }

 void comm_start() {
	usart_set_rx_interrupt_level(&BOARD_USART, USART_INT_LVL_LO);
	usart_set_tx_interrupt_level(&BOARD_USART, USART_INT_LVL_LO);

	comm_temp = tba_create_packet();
 }

 void comm_update() {
	/*if(BOARD_USART.STATUS & USART_RXCIF_bm) {
		comm_handle_rx();
	}
	if(BOARD_USART.STATUS & USART_TXCIF_bm) {
		comm_handle_tx();
	}*/

	if(!cfg_is_disabled(CFG_COMM)) {

		if(comm_rx_state & COMM_RX_DISCARD_bm) {
			comm_rx_state &= ~COMM_RX_DISCARD_bm;
			tba_destroy_packet(comm_temp);
			tba_init_packet(comm_temp);
			
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				if(comm_state != COMM_STATE_INITIALIZED) {
					ioport_set_pin_level(BOARD_USART_XCK, 0);
					comm_state = COMM_STATE_UNINITIALIZED;
				}
			}
		}
		while(rb_can_read(&comm_read_buffer)) {
			if(tba_handle_data(comm_temp, rb_getd(&comm_read_buffer)) == TBA_END) {
				switch(comm_temp->cmd) {
					case TBA_EP_INIT:
						comm_address = tba_get_uint8(comm_temp->data);
						comm_send_device();
						break;
					case TBA_EP_GET_STATE:
						comm_send_state();
						break;
					case TBA_EP_UPDATE_STATE:
						comm_handle_set_target();
						break;
				}
				
				tba_destroy_packet(comm_temp);
				free(comm_temp);
				comm_temp = tba_create_packet();
			}
		}
	}
 }

 void comm_send_cmd(uint8_t cmd) {
	tba_data_t *data = tba_create_data();
	comm_send_data(cmd, data);
	tba_destroy_data(data);
	free(data);
 }

 void comm_send_data(uint8_t cmd, tba_data_t *data) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		rb_putd(&comm_write_buffer, cmd);
		tba_write_data(&comm_write_buffer, data);
		if(comm_tx_state == COMM_TX_IDLE) {
			BOARD_USART.CTRLB |= USART_TXB8_bm;
			usart_put(&BOARD_USART, TBA_DEFAULT_ADDRESS);
			while(!usart_data_register_is_empty(&BOARD_USART)) {} 
			BOARD_USART.CTRLB &= ~USART_TXB8_bm;
			comm_tx_state = COMM_TX_SENDING;
		}
	}
 }

 void comm_handle_protocol_error() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		comm_send_cmd(TBA_NACK);
		rb_clear(&comm_read_buffer);
		comm_rx_state |= COMM_RX_BUSERR_bm;
	}
 }
 
 void comm_handle_set_target() {
	float target = tba_get_float(comm_temp->data);
	ep_set_target(target);
 }

 void comm_send_device() {
	tba_data_t *data = tba_create_data();
	tba_init_array(data, CFG_TYPE == TBA_EP_TYPE_BATTERY ? 7 : 6);

	tba_data_t *temp = tba_get_array(data, 0);
	tba_init_uint8(temp, comm_address);

	temp = tba_get_array(data, 1);
	tba_init_uint16(temp, CFG_DEVICE_ID);

	temp = tba_get_array(data, 2);
	tba_init_uint8(temp, CFG_TYPE);

	temp = tba_get_array(data, 3);
	switch(CFG_TYPE) {
		case TBA_EP_TYPE_CONSUMER:
		case TBA_EP_TYPE_BATTERY:
			tba_init_float(temp, -CFG_POWER_LIMIT);
			break;
		default:
			tba_init_float(temp, 0);
			break;
	}

	temp = tba_get_array(data, 4);
	switch(CFG_TYPE) {
		case TBA_EP_TYPE_GENERATOR:
		case TBA_EP_TYPE_BATTERY:
			tba_init_float(temp, CFG_POWER_LIMIT);
			break;
		default:
			tba_init_float(temp, 0);
			break;
	}

	temp = tba_get_array(data, 5);
	tba_init_float(temp, CFG_MAX_CHANGE);

	if(CFG_TYPE == TBA_EP_TYPE_BATTERY) {
		temp = tba_get_array(data, 6);
		tba_init_float(temp, CFG_BATTERY_MAX);
	}

	comm_send_data(TBA_EP_ADD_DEVICE, data);
	tba_destroy_data(data);
	free(data);
 }

 void comm_send_state() {
	tba_data_t *data = tba_create_data();
	tba_init_array(data, CFG_TYPE == TBA_EP_TYPE_BATTERY ? 3 : 2);

	tba_data_t *temp = tba_get_array(data, 0);
	tba_init_uint8(temp, comm_address);

	temp = tba_get_array(data, 1);
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		float current = sen_last_data->curr;
		if(current < SEN_CURR_RES && current > -SEN_CURR_RES) {
			current = 0;
		}
		float power = current * sen_last_data->vout;
		tba_init_float(temp, power);
	}
	if(CFG_TYPE == TBA_EP_TYPE_BATTERY) {
		temp = tba_get_array(data, 2);
		tba_init_float(temp, ep_get_stored());
	}

	comm_send_data(TBA_EP_UPDATE_STATE, data);
	tba_destroy_data(data);
	free(data);
 }

 ISR(PORTD_INT_vect) {
	if(comm_state == COMM_STATE_INIT) {
		sysclk_disable_module(SYSCLK_PORT_GEN, SYSCLK_RTC);
		comm_state = COMM_STATE_TIMEOUT;
	}
 }

 void comm_handle_rx() {
	if((BOARD_USART.STATUS & (USART_BUFOVF_bm | USART_PERR_bm | USART_FERR_bm)) && !(comm_rx_state & COMM_RX_BUSERR_bm)) {
		rb_clear(&comm_read_buffer);
		comm_rx_state |= COMM_RX_BUSERR_bm | COMM_RX_DISCARD_bm;
	}
	if(BOARD_USART.STATUS & USART_RXB8_bm) {
		comm_rx_state &= ~COMM_RX_BUSERR_bm;
		if(usart_getchar(&BOARD_USART) == CFG_BUSADDR) {
			BOARD_USART.CTRLB &= ~USART_MPCM_bm;
		} else {
			BOARD_USART.CTRLB |= USART_MPCM_bm;
		}
	} else {
		if(!(comm_rx_state & COMM_RX_BUSERR_bm)) {
			rb_put(&comm_read_buffer, &(BOARD_USART.DATA));
		}
	}
 }

 void comm_handle_tx() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(rb_can_read(&comm_write_buffer)) {
			BOARD_USART.DATA = rb_getd(&comm_write_buffer);
		} else {
			comm_tx_state = COMM_TX_IDLE;
			if(comm_state == COMM_STATE_INITIALIZED) {
				ioport_set_pin_level(BOARD_USART_XCK, 0);
			}
		}
	}
	BOARD_USART.STATUS = USART_TXCIF_bm;
 }

 ISR(USARTD0_RXC_vect) {
	comm_handle_rx();
 }

 ISR(USARTD0_TXC_vect) {
	comm_handle_tx();
 }