/*
 * sen_int.h
 *
 * Created: 15.07.2016 11:04:35
 *  Author: Lorenzo Rai
 */ 


#ifndef SEN_INT_H_
#define SEN_INT_H_

#include "sensors.h"

void sen_swap_buffers(void);

void sen_adc_handler(ADC_t *adc, uint8_t ch_mask, adc_result_t res);

#endif /* SEN_INT_H_ */