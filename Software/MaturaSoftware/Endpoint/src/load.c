/*
 * load.c
 *
 * Created: 15.07.2016 15:09:57
 *  Author: Lorenzo Rai
 */ 

 #include <inttypes.h>
 #include <util/atomic.h>
 #include "load.h"
 #include "sensors.h"
 #include "config_manager.h"
 #include "timer.h"

 volatile float load_target = 0;
 uint16_t load_dac_current = 0;
 uint8_t load_should_update = 0;

 void load_init() {
	ioport_set_pin_dir(LOAD_LED_RED, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(LOAD_LED_GREEN, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(BOARD_LOAD_CTRL, IOPORT_DIR_OUTPUT);

	struct dac_config dac_cfg;
	dac_read_configuration(&LOAD_DAC, &dac_cfg);
	dac_set_conversion_parameters(&dac_cfg, DAC_REF_AVCC, DAC_ADJ_RIGHT);
	dac_set_active_channel(&dac_cfg, LOAD_DAC_CH, 0);
	dac_set_conversion_trigger(&dac_cfg, 0, 0);
	dac_write_configuration(&LOAD_DAC, &dac_cfg);
	
	dac_enable(&LOAD_DAC);
 }

 void load_start() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		dac_wait_for_channel_ready(&LOAD_DAC, LOAD_DAC_CH);
		load_dac_current = LOAD_BOTTOM;
		dac_set_channel_value(&LOAD_DAC, LOAD_DAC_CH, LOAD_BOTTOM);
		ioport_set_pin_level(LOAD_LED_GREEN, 1);
		ioport_set_pin_level(LOAD_LED_RED, 0);
	}
 }

 void load_stop() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		dac_wait_for_channel_ready(&LOAD_DAC, LOAD_DAC_CH);
		dac_set_channel_value(&LOAD_DAC, LOAD_DAC_CH, 0);
		ioport_set_pin_level(LOAD_LED_GREEN, 0);
		ioport_set_pin_level(LOAD_LED_RED, 0);
	}
 }

 void load_new_data() {
	load_should_update = 1;
	load_update();
 }

 void load_set_power(float target) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		load_target = target;
	}
 }

 void load_update() {
	float power_target = 0;
	if(timer_using_profile()) {
		power_target = timer_get_current_target();
	} else {
		power_target = load_target;
	}

	if(!cfg_is_disabled(CFG_LOAD) && power_target < 0) {
		if(load_should_update) {
			load_should_update = 0;
			
			float current_state = sen_last_data->curr * sen_last_data->vout;
			float delta = -(power_target - current_state);
			float dnorm = delta / LOAD_POWER_MAX;
			float change = dnorm * (LOAD_TOP - LOAD_BOTTOM);

			if(-change > load_dac_current) {
				load_dac_current = 0;
				ioport_set_pin_level(LOAD_LED_GREEN, 1);
				ioport_set_pin_level(LOAD_LED_RED, 1);
			} else if(change > (LOAD_TOP - load_dac_current)) {
				load_dac_current = LOAD_TOP;
				ioport_set_pin_level(LOAD_LED_GREEN, 0);
				ioport_set_pin_level(LOAD_LED_RED, 1);
			} else {
				load_dac_current += change;
				ioport_set_pin_level(LOAD_LED_GREEN, 1);
				ioport_set_pin_level(LOAD_LED_RED, 0);
			}

			dac_wait_for_channel_ready(&LOAD_DAC, LOAD_DAC_CH);
			dac_set_channel_value(&LOAD_DAC, LOAD_DAC_CH, load_dac_current);
		}
	} else {
		dac_wait_for_channel_ready(&LOAD_DAC, LOAD_DAC_CH);
		dac_set_channel_value(&LOAD_DAC, LOAD_DAC_CH, 0);
		ioport_set_pin_level(LOAD_LED_GREEN, 0);
		ioport_set_pin_level(LOAD_LED_RED, 0);
	}
 }