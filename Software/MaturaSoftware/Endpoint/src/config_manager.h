/*
 * config_manager.h
 *
 * Created: 12.07.2016 17:27:10
 *  Author: Lorenzo Rai
 */ 


#ifndef CONFIG_MANAGER_H_
#define CONFIG_MANAGER_H_

#include <avr/io.h>
#include <avr/eeprom.h>

#define CFG_TIMED_ARRAY_SIZE		100

extern volatile uint8_t cfg_nmeep_busid;
extern volatile uint16_t cfg_nmeep_deviceid;
extern volatile uint8_t cfg_nmeep_type;
extern volatile uint8_t cfg_nmeep_enable_mask;
extern volatile float cfg_nmeep_voltage_target;
extern volatile float cfg_nmeep_current_limit;
extern volatile float cfg_nmeep_max_change;
extern volatile float cfg_nmeep_power_limit;
extern volatile uint16_t cfg_nmeep_regulator_top;
extern volatile uint16_t cfg_nmeep_regulator_max;
extern volatile uint16_t cfg_nmeep_regulator_min;
extern volatile float cfg_nmeep_battery_max;
extern volatile float cfg_nmeep_timed_array[CFG_TIMED_ARRAY_SIZE];
extern volatile uint8_t cfg_nmeep_using_timed;
extern volatile uint8_t cfg_nmeep_busaddr;

extern volatile float cfg_power_target;

#define CFG_BUSID					(*(uint8_t *) (((uint8_t *) &cfg_nmeep_busid) + MAPPED_EEPROM_START))
#define CFG_DEVICE_ID				(*(uint16_t *) (((uint8_t *) &cfg_nmeep_deviceid) + MAPPED_EEPROM_START))
#define CFG_TYPE					(*(uint8_t *) (((uint8_t *) &cfg_nmeep_type) + MAPPED_EEPROM_START))
#define CFG_ENABLE_MASK				(*(uint8_t *) (((uint8_t *) &cfg_nmeep_enable_mask) + MAPPED_EEPROM_START))
#define CFG_VOLTAGE_TARGET			(*(float *) (((uint8_t *) &cfg_nmeep_voltage_target) + MAPPED_EEPROM_START))
#define CFG_CURRENT_LIMIT			(*(float *) (((uint8_t *) &cfg_nmeep_current_limit) + MAPPED_EEPROM_START))
#define CFG_MAX_CHANGE				(*(float *) (((uint8_t *) &cfg_nmeep_max_change) + MAPPED_EEPROM_START))
#define CFG_POWER_LIMIT				(*(float *) (((uint8_t *) &cfg_nmeep_power_limit) + MAPPED_EEPROM_START))
#define CFG_REGULATOR_TOP			(*(uint16_t *) (((uint8_t *) &cfg_nmeep_regulator_top) + MAPPED_EEPROM_START))
#define CFG_REGULATOR_MAX			(*(uint16_t *) (((uint8_t *) &cfg_nmeep_regulator_max) + MAPPED_EEPROM_START))
#define CFG_REGULATOR_MIN			(*(uint16_t *) (((uint8_t *) &cfg_nmeep_regulator_min) + MAPPED_EEPROM_START))
#define CFG_BATTERY_MAX				(*(float *) (((uint8_t *) &cfg_nmeep_battery_max) + MAPPED_EEPROM_START))
#define CFG_TIMED_ARRAY				(*(float *) (((uint8_t *) &cfg_nmeep_timed_array[0]) + MAPPED_EEPROM_START))
#define CFG_USING_TIMED				(*(uint8_t *) (((uint8_t *) &cfg_nmeep_using_timed) + MAPPED_EEPROM_START))
#define CFG_BUSADDR					(*(uint8_t *) (((uint8_t *) &cfg_nmeep_busaddr) + MAPPED_EEPROM_START))

enum CFG_PERIPHERALS_enum {
	CFG_LOAD = 0x01, CFG_REGULATOR = 0x02, CFG_COMM = 0x04
};

uint8_t cfg_is_disabled(enum CFG_PERIPHERALS_enum per);

void cfg_disable(enum CFG_PERIPHERALS_enum per);

void cfg_enable(enum CFG_PERIPHERALS_enum per);

#endif /* CONFIG_MANAGER_H_ */