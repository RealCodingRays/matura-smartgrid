/*
 * load.h
 *
 * Created: 15.07.2016 15:06:38
 *  Author: Lorenzo Rai
 */ 


#ifndef LOAD_H_
#define LOAD_H_

#include <asf.h>
#include <avr/io.h>

#define LOAD_DAC			DACA
#define LOAD_DAC_CH			DAC_CH0

#define LOAD_LED_RED		IOPORT_CREATE_PIN(PORTC, 4)
#define LOAD_LED_GREEN		IOPORT_CREATE_PIN(PORTC, 5)
#define LOAD_CTRL			IOPORT_CREATE_PIN(PORTA, 2)

#define LOAD_POWER_MAX			8
#define LOAD_TOP			0x0FFF
#define LOAD_BOTTOM			0x09B2

void load_init(void);

void load_start(void);

void load_stop(void);

void load_new_data(void);

void load_set_power(float target);

void load_update(void);

#endif /* LOAD_H_ */