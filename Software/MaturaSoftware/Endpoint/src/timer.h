/*
 * timer.h
 *
 * Created: 16.07.2016 12:45:42
 *  Author: Lorenzo Rai
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#include <asf.h>
#include <inttypes.h>
#include "regulator.h"
#include "load.h"

#define TIMER_TC_CLK			TCC4
#define TIMER_TC_TIME			TCC5
#define TIMER_DIV				TC45_CLKSEL_DIV1024_gc
#define TIMER_INTERVAL			0x7A12

void timer_init(void);

void timer_start(void);

void timer_stop(void);

void timer_reset(void);

void timer_set_profile(float data[], size_t size);

float timer_get_current_target(void);

uint8_t timer_using_profile(void);

#endif /* TIMER_H_ */