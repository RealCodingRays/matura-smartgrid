/**
 * \file
 *
 * \brief User board definition template
 *
 */

 /* This file is intended to contain definitions and configuration details for
 * features and devices that are available on the board, e.g., frequency and
 * startup time for an external crystal, external memory devices, LED and USART
 * pins.
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef USER_BOARD_H
#define USER_BOARD_H

#include <conf_board.h>

// External oscillator settings.
// Uncomment and set correct values if external oscillator is used.

// External oscillator frequency
//#define BOARD_XOSC_HZ          8000000

// External oscillator type.
//!< External clock signal
//#define BOARD_XOSC_TYPE        XOSC_TYPE_EXTERNAL
//!< 32.768 kHz resonator on TOSC
//#define BOARD_XOSC_TYPE        XOSC_TYPE_32KHZ
//!< 0.4 to 16 MHz resonator on XTALS
//#define BOARD_XOSC_TYPE        XOSC_TYPE_XTAL

// External oscillator startup time
//#define BOARD_XOSC_STARTUP_US  500000

#define BOARD_CURR_R		0.5
#define BOARD_VDIV_R1		100000
#define BOARD_VDIV_R2		4640

#define BOARD_LOAD_CTRL			IOPORT_CREATE_PIN(PORTA, 2)
#define BOARD_ISENIN			IOPORT_CREATE_PIN(PORTA, 3)
#define BOARD_ISENOUT			IOPORT_CREATE_PIN(PORTA, 4)
#define BOARD_REG_VSEN			IOPORT_CREATE_PIN(PORTD, 5)
#define BOARD_REG_CTRL			IOPORT_CREATE_PIN(PORTD, 4)
#define BOARD_EDG_TXD			IOPORT_CREATE_PIN(PORTD, 3)
#define BOARD_EDG_RXD			IOPORT_CREATE_PIN(PORTD, 2)
#define BOARD_EDG_XCK			IOPORT_CREATE_PIN(PORTD, 1)
#define BOARD_DEBUG_SDA			IOPORT_CREATE_PIN(PORTC, 0)
#define BOARD_DEBUG_SCL			IOPORT_CREATE_PIN(PORTC, 1)
#define BOARD_REG_DISABLE		IOPORT_CREATE_PIN(PORTC, 2)
#define BOARD_LOAD_DISABLE		IOPORT_CREATE_PIN(PORTC, 3)
#define BOARD_LOAD_LED_RED		IOPORT_CREATE_PIN(PORTC, 4)
#define BOARD_LOAD_LED_GREEN	IOPORT_CREATE_PIN(PORTC, 5)
#define BOARD_REG_LED_RED		IOPORT_CREATE_PIN(PORTC, 6)
#define BOARD_REG_LED_GREEN		IOPORT_CREATE_PIN(PORTC, 7)

#endif // USER_BOARD_H
