/*
 * sensors.h
 *
 * Created: 13.07.2016 14:53:49
 *  Author: Lorenzo Rai
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

#include <avr/io.h>
#include <asf.h>

#define SEN_VDIV_FACTOR		(1.0/(((float) BOARD_VDIV_R2) / ((float) (BOARD_VDIV_R1 + BOARD_VDIV_R2))))
#define SEN_GAIN_FACTOR		(64)
#define SEN_CURR_RES		0.05

#define SEN_VSENS			IOPORT_CREATE_PIN(PORTD, 5)
#define SEN_ISENSIN			IOPORT_CREATE_PIN(PORTA, 3)
#define SEN_ISENSOUT		IOPORT_CREATE_PIN(PORTA, 4)

#define SEN_VSENS_ADC		ADCCH_POS_PIN13
#define SEN_VSENS_NEG_ADC	ADCCH_NEG_PAD_GND
#define SEN_ISENSIN_ADC		ADCCH_POS_PIN3
#define SEN_ISENSOUT_ADC	ADCCH_NEG_PIN4

#define SEN_ADC_OFFSET_ADDR	0x00
#define SEN_ADC_GAIN_ADDR	0x02

#define SEN_RES_MAX			2047
#define SEN_RES_MIN			-2047

struct sen_data_struct {
	float curr;
	float vout;
	float vreg;
};

extern volatile struct sen_data_struct *sen_last_data;

void sen_init(void);

void sen_start(void);

void sen_stop(void);

#endif /* SENSORS_H_ */