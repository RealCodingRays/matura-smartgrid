/*
 * config_manager.c
 *
 * Created: 14.07.2016 11:02:31
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <avr/io.h>
 #include <avr/eeprom.h>
 #include <inttypes.h>
 #include <float.h>
 #include <common/tba.h>
 #include "config_manager.h"

 volatile uint8_t EEMEM cfg_nmeep_busid = TBA_DEFAULT_ADDRESS;
 volatile uint16_t EEMEM cfg_nmeep_deviceid = 2;
 volatile uint8_t EEMEM cfg_nmeep_type = TBA_EP_TYPE_GENERATOR;
 volatile uint8_t EEMEM cfg_nmeep_enable_mask = 0b00000111;
 volatile float EEMEM cfg_nmeep_voltage_target = 20.0;
 volatile float EEMEM cfg_nmeep_current_limit = 1.0;
 volatile float EEMEM cfg_nmeep_max_change = 0.5;
 volatile float EEMEM cfg_nmeep_power_limit = 20.0;
 volatile uint16_t EEMEM cfg_nmeep_regulator_top = 0x012C;
 volatile uint16_t EEMEM cfg_nmeep_regulator_max = 0x0104;
 volatile uint16_t EEMEM cfg_nmeep_regulator_min = 0x0001;
 volatile float EEMEM cfg_nmeep_battery_max = 400.0;
 volatile float EEMEM cfg_nmeep_timed_array[CFG_TIMED_ARRAY_SIZE] = {};
 volatile uint8_t EEMEM cfg_nmeep_using_timed = 0;
 volatile uint8_t EEMEM cfg_nmeep_busaddr = 2;

 volatile uint8_t cfg_disable_mask = 0xFF;

 uint8_t cfg_is_disabled(enum CFG_PERIPHERALS_enum per) {
	switch(per) {
		case CFG_LOAD:
			if(ioport_get_pin_level(BOARD_LOAD_DISABLE) == 1) {
				//return 1;
			}
			break;
		case CFG_REGULATOR:
			if(ioport_get_pin_level(BOARD_REG_DISABLE) == 1) {
				//return 1;
			}
			break;
		default:
			break;
	}
	if((CFG_ENABLE_MASK & per) == 0) {
		return 1;
	}
	return (cfg_disable_mask & per);
 }

 void cfg_disable(enum CFG_PERIPHERALS_enum per) {
	cfg_disable_mask |= per;
 }

 void cfg_enable(enum CFG_PERIPHERALS_enum per) {
	cfg_disable_mask &= ~per;
 }