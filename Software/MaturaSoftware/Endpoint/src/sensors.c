/*
 * sensors.c
 *
 * Created: 13.07.2016 15:15:26
 *  Author: Lorenzo Rai
 */ 

 #include <util/atomic.h>
 #include "sensors.h"
 #include "sen_int.h"
 #include "regulator.h"
 #include "load.h"
 
 struct adc_channel_config sen_adcch_curr_cfg;
 struct adc_channel_config sen_adcch_vout_cfg;
 struct adc_channel_config sen_adcch_vreg_cfg;

 volatile uint8_t sen_enable = 0;
 volatile uint8_t sen_current = 0;

 volatile struct sen_data_struct *sen_last_data;
 volatile struct sen_data_struct *sen_buffer_data;
 volatile struct sen_data_struct sen_buffer1_data;
 volatile struct sen_data_struct sen_buffer2_data;

 void sen_init() {
	ioport_set_pin_sense_mode(SEN_VSENS, PORT_ISC_INPUT_DISABLE_gc);
	ioport_set_pin_sense_mode(SEN_ISENSIN, PORT_ISC_INPUT_DISABLE_gc);
	ioport_set_pin_sense_mode(SEN_ISENSOUT, PORT_ISC_INPUT_DISABLE_gc);

	struct adc_config adc_cfg;
	
	adc_enable(&ADCA);
	adc_read_configuration(&ADCA, &adc_cfg);
	adcch_read_configuration(&ADCA, ADC_CH0, &sen_adcch_curr_cfg);
	adcch_read_configuration(&ADCA, ADC_CH0, &sen_adcch_vout_cfg);
	adcch_read_configuration(&ADCA, ADC_CH0, &sen_adcch_vreg_cfg);

	adc_set_conversion_parameters(&adc_cfg, ADC_SIGN_ON, ADC_RES_12, ADC_REF_BANDGAP);
	adc_set_conversion_trigger(&adc_cfg, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_cfg, 200000UL);
	adc_set_callback(&ADCA, &sen_adc_handler);
	adc_write_configuration(&ADCA, &adc_cfg);
	
	adcch_enable_oversampling(&sen_adcch_curr_cfg, ADC_SAMPNUM_8X, 12);
	adcch_enable_oversampling(&sen_adcch_vout_cfg, ADC_SAMPNUM_8X, 12);
	adcch_enable_oversampling(&sen_adcch_vreg_cfg, ADC_SAMPNUM_8X, 12);
	
	adcch_set_input(&sen_adcch_curr_cfg, SEN_ISENSIN_ADC, SEN_ISENSOUT_ADC, SEN_GAIN_FACTOR);
	adcch_set_input(&sen_adcch_vout_cfg, SEN_ISENSOUT_ADC, SEN_VSENS_NEG_ADC, 1);
	adcch_set_input(&sen_adcch_vreg_cfg, SEN_VSENS_ADC, SEN_VSENS_NEG_ADC, 1);
	
	adcch_enable_interrupt(&sen_adcch_curr_cfg);
	adcch_enable_interrupt(&sen_adcch_vout_cfg);
	adcch_enable_interrupt(&sen_adcch_vreg_cfg);

	//adcch_enable_correction(&sen_adcch_vreg_cfg, ((int16_t) 0xffee), ((int16_t) 0x06d4), ((int16_t) 0x05d9));
	//adcch_enable_correction(&sen_adcch_vout_cfg, ((int16_t) 0xffee), ((int16_t) 0x06d4), ((int16_t) 0x05d9));
	adcch_enable_correction(&sen_adcch_curr_cfg, ((int16_t) 0xfb64), ((int16_t) 0x06d4), ((int16_t) 0x05d9));
	sen_adcch_curr_cfg.gaincorr0 = 2374;
	sen_adcch_curr_cfg.gaincorr1 = 2374 >> 8;

	sen_last_data = &sen_buffer1_data;
	sen_buffer_data = &sen_buffer2_data;
 }

 void sen_start() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		sen_enable = 1;
		sen_current = 1;
		adcch_write_configuration(&ADCA, ADC_CH0, &sen_adcch_curr_cfg);
		adc_start_conversion(&ADCA, ADC_CH0);
	}
 }

 void sen_stop() {
	sen_enable = 0;
 }

 void sen_adc_handler(ADC_t *adc, uint8_t ch_mask, adc_result_t res) {
	switch(sen_current) {
		case 1:
			sen_buffer_data->curr = (((((float) res)/((float) SEN_GAIN_FACTOR))/((float) SEN_RES_MAX)) * SEN_VDIV_FACTOR) / BOARD_CURR_R;
			adcch_write_configuration(&ADCA, ADC_CH0, &sen_adcch_vout_cfg);
			sen_current++;
			break;
		case 2:
			sen_buffer_data->vout = (((float) res)/((float) SEN_RES_MAX)) * SEN_VDIV_FACTOR;
			adcch_write_configuration(&ADCA, ADC_CH0, &sen_adcch_vreg_cfg);
			sen_current++;
			break;
		case 3:
			sen_buffer_data->vreg = (((float) res)/((float) SEN_RES_MAX)) * SEN_VDIV_FACTOR;
			sen_current++;
			break;
	}

	if(sen_current == 4) {
		sen_swap_buffers();
		reg_new_data();
		load_new_data();
		if(sen_enable) {
			sen_current = 1;
			adcch_write_configuration(&ADCA, ADC_CH0, &sen_adcch_curr_cfg);
			adc_start_conversion(&ADCA, ADC_CH0);
		}
	} else {
		adc_start_conversion(&ADCA, ADC_CH0);
	}
 }

 void sen_swap_buffers() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		volatile struct sen_data_struct *buffer = sen_buffer_data;
		sen_buffer_data = sen_last_data;
		sen_last_data = buffer;
	}
 }