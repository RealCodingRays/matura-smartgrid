/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <common/tba.h>
#include <avr/eeprom.h>
#include <interrupt.h>
#include <avr/io.h>
#include "load.h"
#include "regulator.h"
#include "sensors.h"
#include "timer.h"
#include "config_manager.h"
#include "comm.h"
#include "common/ring_buffer.h"
#include "endpoint.h"

volatile float current_target;

int main (void) {
	sysclk_init();
	eeprom_enable_mapping();
	board_init();
	pmic_init();
	pmic_set_scheduling(PMIC_SCH_ROUND_ROBIN);
	sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_EVSYS);
	
	cfg_enable(CFG_COMM);
	switch(CFG_TYPE) {
		case TBA_EP_TYPE_BATTERY:
		cfg_enable(CFG_REGULATOR | CFG_LOAD);
		break;
		case TBA_EP_TYPE_CONSUMER:
		cfg_enable(CFG_LOAD);
		break;
		case TBA_EP_TYPE_GENERATOR:
		cfg_enable(CFG_REGULATOR);
		break;
	}

	sen_init();
	reg_init();
	load_init();
	timer_init();
	comm_init();
	ep_init();

	if(CFG_USING_TIMED) {
		timer_set_profile(&CFG_TIMED_ARRAY, CFG_TIMED_ARRAY_SIZE);
	}

	cpu_irq_enable();
	reg_start();
	load_start();
	sen_start();
	timer_start();
	comm_start();
	ep_start();

	load_set_power(-5.0);
	reg_set_target(20.0);

	while(1) {
		comm_update();
		reg_update();
	}
}
