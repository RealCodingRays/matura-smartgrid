/*
 * regulator.h
 *
 * Created: 13.07.2016 12:04:15
 *  Author: Lorenzo Rai
 */ 


#ifndef REGULATOR_H_
#define REGULATOR_H_

#include <avr/io.h>
#include <asf.h>

#define REG_TC					TCD5
#define REG_CC					TC45_CCA
#define REG_LED_RED				IOPORT_CREATE_PIN(PORTC, 6)
#define REG_LED_GREEN			IOPORT_CREATE_PIN(PORTC, 7)
#define REG_CTRL				IOPORT_CREATE_PIN(PORTD, 4)

#define REG_VOLTAGE_INPUT		12.0
#define REG_VOLTAGE_MIN			15.0

void reg_init(void);

void reg_reload_config(void);

void reg_set_target(float power);

float reg_get_target(void);

void reg_start(void);

void reg_stop(void);

void reg_new_data(void);

void reg_update(void);

#endif /* REGULATOR_H_ */