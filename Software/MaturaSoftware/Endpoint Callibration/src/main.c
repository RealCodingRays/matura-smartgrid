/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include <avr/io.h>
#include <inttypes.h>
#include <interrupt.h>

volatile uint8_t stage = 0;
volatile int16_t gain;
volatile int16_t offset;

#define EXP_VALUE		1800.0

ISR(ADCA_CH0_vect) {
	volatile int16_t res = ADCA.CH0.RES;
	switch(stage) {
		case 0:
			offset = ADCA.CH0.RES;
			ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN4_gc | ADC_CH_MUXNEGH_GND_gc;
			ADCA.CTRLA |= ADC_START_bm;
			stage = 1;
			break;
		case 1:
			gain = (int16_t)((EXP_VALUE * 2048.0)/((float) (res - offset)));
			ADCA.CH0.OFFSETCORR0 = offset;
			ADCA.CH0.OFFSETCORR1 = (offset >> 8);
			ADCA.CH0.GAINCORR0 = gain;
			ADCA.CH0.GAINCORR1 = (gain >> 8);
			ADCA.CH0.CORRCTRL = ADC_CH_CORREN_bm;
			ADCA.CTRLA |= ADC_START_bm;
			stage = 2;
			break;
		case 2:
			ADCA.CTRLA |= ADC_START_bm;
			res++;
			break;
	}
}

int main (void)
{
	sysclk_init();

	board_init();
	pmic_init();

	ioport_set_pin_sense_mode(IOPORT_CREATE_PIN(PORTA, 3), PORT_ISC_INPUT_DISABLE_gc);
	ioport_set_pin_sense_mode(IOPORT_CREATE_PIN(PORTA, 4), PORT_ISC_INPUT_DISABLE_gc);

	sysclk_enable_module(SYSCLK_PORT_A, SYSCLK_ADC);

	stage = 0;
	ADCA.CTRLA = ADC_ENABLE_bm;
	ADCA.CTRLB = ADC_CONMODE_bm;
	ADCA.REFCTRL = ADC_BANDGAP_bm | ADC_REFSEL_INT1V_gc;
	ADCA.PRESCALER = ADC_PRESCALER_DIV32_gc;
	ADCA.SAMPCTRL = 16;
	ADCA.CH0.CTRL = ADC_CH_INPUTMODE_DIFFWGAINH_gc;
	ADCA.CH0.MUXCTRL = ADC_CH_MUXPOS_PIN4_gc | ADC_CH_MUXNEGH_PIN4_gc;
	ADCA.CH0.INTCTRL = ADC_CH_INTLVL_HI_gc;
	ADCA.CTRLA |= ADC_START_bm;
	cpu_irq_enable();
	while(1) {}
}
