/*
 * logger.h
 *
 * Created: 05.08.2016 16:26:22
 *  Author: Lorenzo Rai
 */ 


#ifndef LOGGER_H_
#define LOGGER_H_

#include <avr/eeprom.h>

struct log_log_struct {
	linked_list_t *list;
	uint8_t *dest;
	uint8_t src;
	uint32_t time;
	char *msg;
};

void log_init(void);

void log_update(void);

void log_msg(uint8_t src, char *msg);

void log_write(struct log_log_struct* msg);

#endif /* LOGGER_H_ */