/*
 * history.c
 *
 * Created: 11.08.2016 13:31:14
 *  Author: Lorenzo Rai
 */ 

 #include <util/atomic.h>
 #include <math.h>
 #include <stdlib.h>
 #include <stddef.h>
 #include <daytime.h>
 #include "history.h"

 void hist_init(history_t *history) {
	history->running = 0;
	history->max_change = 0;
	history->old_load = 0;
	history->current = NULL;
	ll_init(&(history->immediate));
	ll_init(&(history->active));
	ll_init(&(history->inactive));
 }

 void hist_set_max(history_t *history, float max_change) {
	history->max_change = max_change;
 }

 void hist_handle_data(history_t *history, float load) {
	float delta = fabsf(load - history->old_load);
	if((delta > history->max_change) || history->running) {
		if(delta <= history->max_change) {
			history->running--;
		}

		if(history->current == NULL) {
			history->current = hist_create_event();
		}

		hist_append_data(history->current, load);

	} else if(history->current != NULL) {
		linked_list_t *elem;
		if(!ll_is_empty(&(history->immediate))) {
			elem = history->immediate.next;
			while(elem != &(history->immediate)) {
				if(hist_get_correlation(ll_get_elem(elem, history_event_t, list), history->current)) {
					history_event_t *evt = ll_get_elem(elem, history_event_t, list);
					hist_join_evts(evt, history->current);
					evt->counter++;
					if(evt->counter >= HIST_IMMEDIATE_CNT) {
						ll_remove(elem);
						hist_update_reaction_time(evt, history->max_change, (((float) dtime_timer_interval) / 60) / 60);
						ll_add_before(&(history->active), elem);
						evt->counter = 0;
					}

					free(history->current->data);
					free(history->current);
					history->current = NULL;
					return;
				}
				elem = elem->next;
			}
		}

		if(!ll_is_empty(&(history->active))) {
			elem = history->active.next;
			while(elem != &(history->active)) {
				if(hist_get_correlation(ll_get_elem(elem, history_event_t, list), history->current)) {
					hist_join_evts(ll_get_elem(elem, history_event_t, list), history->current);
					free(history->current->data);
					free(history->current);
					history->current = NULL;
					return;
				}
				elem = elem->next;
			}
		}

		if(!ll_is_empty(&(history->inactive))) {
			elem = history->inactive.next;
			while(elem != &(history->inactive)) {
				if(hist_get_correlation(ll_get_elem(elem, history_event_t, list), history->current)) {
					history_event_t *evt = ll_get_elem(elem, history_event_t, list);
					hist_join_evts(evt, history->current);
					evt->counter++;
					if(evt->counter >= HIST_INACTIVE_CNT) {
						ll_remove(elem);
						ll_add_before(&(history->active), elem);
						evt->counter = 0;
					}

					free(history->current->data);
					free(history->current);
					history->current = NULL;
					return;
				}
				elem = elem->next;
			}
		}

		ll_add_before(&(history->immediate), &(history->current->list));
		history->current = NULL;
	}
 }


 history_event_t *hist_create_event() {
	history_event_t *evt = malloc(sizeof(history_event_t));
	evt->counter = 0;
	evt->size = 0;
	evt->time_of_day = dtime_cut_day();
	evt->reaction_time = 0;
	evt->array_size = HIST_SIZE_STEP;
	evt->data = malloc(sizeof(float) * HIST_SIZE_STEP);
	return evt;
 }

 void hist_append_data(history_event_t *evt, float data) {
	if((evt->size + 1) >= evt->array_size) {
		evt->array_size += HIST_SIZE_STEP;
		realloc(evt->data, evt->array_size);
	}
	*(evt->data + evt->size) = data;
	evt->size++;
 }

 uint8_t hist_get_correlation(history_event_t *evt1, history_event_t *evt2) {
	if((evt2->time_of_day < (evt1->time_of_day - (evt1->time_of_day * HIST_TIME_TOLERANCE))) || (evt2->time_of_day > (evt1->time_of_day + (evt1->time_of_day * HIST_TIME_TOLERANCE)))) {
		return 0;
	}
	if((evt2->size < (evt1->size - (evt1->size * HIST_X_TOLERANCE + 1))) || (evt2->size > (evt1->size + (evt1->size * HIST_X_TOLERANCE + 1)))) {
		return 0;
	}
	float* scaled_evt2 = hist_scale_event(evt2, evt1->size);
	uint8_t i;
	for(i = 0; i < evt1->size; i++) {
		if((scaled_evt2[i] > (evt1->data[i] + (evt1->data[i] * HIST_Y_TOLERANCE))) || (scaled_evt2[i] < (evt1->data[i] - (evt1->data[i] * HIST_Y_TOLERANCE)))) {
			free(scaled_evt2);
			return 0;
		}
	}
	free(scaled_evt2);
	return 1;
 }

 float *hist_scale_event(history_event_t *evt, uint8_t new_size) {
	float *ret = malloc(sizeof(float) * new_size);
	float factor = ((float) evt->size) / ((float) new_size);
	for(uint8_t i = 0; i < new_size; i++) {
		float npos = factor * i;
		float dx = fmodf(npos, 1);
		uint8_t oi = (uint8_t) (npos - dx);
		if(dx == 0) {
			ret[i] = evt->data[oi];
		} else {
			ret[i] = ((evt->data[oi + 1] - evt->data[oi]) * dx) + evt->data[oi];
		}
	}
	return ret;
 }

 void hist_join_evts(history_event_t *evt1, history_event_t *evt2) {
	uint8_t nsize = roundf(((float) evt1->size + evt2->size)/(2));
	float* scaled_evt1 = hist_scale_event(evt1, nsize);
	float* scaled_evt2 = hist_scale_event(evt2, nsize);
	float* new_data = malloc(sizeof(float) * nsize);
	for(uint8_t i = 0; i < nsize; i++) {
		new_data[i] = (scaled_evt1[i] + scaled_evt2[i]) / 2;
	}
	free(scaled_evt1);
	free(scaled_evt2);
	free(evt1->data);
	evt1->time_of_day = (evt1->time_of_day + evt2->time_of_day) / 2;
	evt1->array_size = nsize;
	evt1->size = nsize;
	evt1->data = new_data;
 }

 void hist_update_reaction_time(history_event_t *evt, float dydt, float dt) {
	if(evt->size == 0) {
		return;
	}
	float pwr = evt->data[evt->size - 1];
	for(int16_t i = evt->size - 2; i >= 0; i--) {
		pwr -= dt * dydt;
		if(pwr <= evt->data[i]) {
			pwr = evt->data[i];
		}
	}
	float rtime_h = (1/dydt) * pwr;
	uint16_t rtime_s = roundf(rtime_h * 60 * 60);
	if(rtime_s > evt->time_of_day) {
		evt->reaction_time = ((uint16_t) 60 * 60 * 24) - (rtime_s - evt->time_of_day);
	} else {
		evt->reaction_time = evt->time_of_day - rtime_s;
	}
 }


 float hist_get_prediction(history_t *history) {
	uint16_t time = dtime_cut_day();
	linked_list_t *elem = history->active.next;
	float trg = 0;
	while(elem != &(history->active)) {
		history_event_t *evt = ll_get_elem(elem, history_event_t, list);
		if((evt->reaction_time >= time) && (evt->time_of_day < time)) {
			trg += (time - evt->reaction_time) * history->max_change;
		}
		elem = elem->next;
	}
	return trg;
 }