/*
 * history.h
 *
 * Created: 11.08.2016 13:15:22
 *  Author: Lorenzo Rai
 */ 


#ifndef HISTORY_H_
#define HISTORY_H_

#include <inttypes.h>
#include <common/linked_list.h>

#define HIST_SIZE_STEP				10
#define HIST_TIME_TOLERANCE 		0.05
#define HIST_Y_TOLERANCE			0.1
#define HIST_X_TOLERANCE			0.1

#define HIST_IMMEDIATE_CNT			5
#define HIST_INACTIVE_CNT			2

typedef struct history_event_struct {
	linked_list_t list;
	uint8_t counter;
	uint16_t time_of_day;
	uint16_t reaction_time;
	uint8_t size;
	uint8_t array_size;
	float *data;
} history_event_t;

typedef struct history_struct {
	uint8_t running;
	float max_change;
	float old_load;
	history_event_t *current;
	linked_list_t immediate;
	linked_list_t active;
	linked_list_t inactive;
} history_t;

void hist_init(history_t *history);

void hist_set_max(history_t *history, float max_change);

void hist_handle_data(history_t *history, float load);


history_event_t *hist_create_event(void);

void hist_append_data(history_event_t *evt, float data);

void hist_join_evts(history_event_t *evt1, history_event_t *evt2);

uint8_t hist_get_correlation(history_event_t *evt1, history_event_t *evt2);

float *hist_scale_event(history_event_t *evt, uint8_t new_size);

void hist_update_reaction_time(history_event_t *evt, float dydt, float dt);


float hist_get_prediction(history_t *history);

#endif /* HISTORY_H_ */