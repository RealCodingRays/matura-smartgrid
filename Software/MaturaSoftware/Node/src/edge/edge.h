/*
 * edge.h
 *
 * Created: 27.07.2016 14:38:01
 *  Author: Lorenzo Rai
 */ 


#ifndef EDGE_H_
#define EDGE_H_

#include <common/ring_buffer.h>
#include <common/linked_list.h>
#include <common/tba.h>
#include <asf.h>
#include "comm_driver.h"
#include "endpoints.h"
#include "history.h"

struct edge_config_struct {
	USART_t *edg_usart;
	USART_t *per_usart;
	TC0_t *timer0;
	TC1_t *timer1;

	ioport_pin_t edg_rdx;
	ioport_pin_t edg_tdx;
	ioport_pin_t edg_xck; 

	ioport_pin_t per_rdx;
	ioport_pin_t per_tdx;
	ioport_pin_t per_xck;

	ioport_pin_t open;
	ioport_pin_t close;

	ioport_pin_t out_ctrla;
	ioport_pin_t out_ctrlb;
	ioport_pin_t in_ctrla;
	ioport_pin_t in_ctrlb;

	ioport_pin_t sled_red;
	ioport_pin_t sled_green;
	ioport_pin_t vled_red;
	ioport_pin_t vled_green;
};

extern const struct edge_config_struct edge1_cfg;

extern const struct edge_config_struct edge2_cfg;

extern const struct edge_config_struct edge3_cfg;

typedef struct edge_state_struct {
	const struct edge_config_struct *cfg;
	uint8_t enabled;
	uint8_t ctrl_state;
	uint8_t led_state;
	uint8_t edge_state;
	uint8_t get_state;
	uint32_t timer;
	linked_list_t requests;
	commd_t edg_comm;
	commd_t per_comm;
	uint8_t temp;
	uint8_t max_addresses;
	uint8_t next_address;
	endpoints_t endpoints;
	history_t history;
} edge_state_t;

edge_state_t *edge_init(const struct edge_config_struct *cfg);

void edge_start(edge_state_t *edge, uint8_t enable);

void edge_update(edge_state_t *edge);

void edge_enable(edge_state_t *edge);

void edge_disable(edge_state_t *edge);

#endif /* EDGE_H_ */