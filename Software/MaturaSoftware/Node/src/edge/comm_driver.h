/*
 * comm_driver.h
 *
 * Created: 03.08.2016 19:18:30
 *  Author: Lorenzo Rai
 */ 


#ifndef COMM_DRIVER_H_
#define COMM_DRIVER_H_

#include <asf.h>
#include <inttypes.h>
#include <common/tba.h>

#define COMMD_FLAG_KEEP_DATA				0b00100000
#define COMMD_FLAG_NOCALLBACK				0b00000001
#define COMMD_FLAG_NOTIMEOUT				0b00000010
#define COMMD_FLAG_NOANSWER					0b00000100

#define COMMD_SUCCESS						ERROR_SUCCESS
#define COMMD_ERR_COMM_BUSY					0xA1
#define COMMD_ERR_BUFF_OVF					0xA2
#define COMMD_ERR_MALLOC_FAILED				0xA3
#define COMMD_ERR_UNKNOWN					0xA4
#define COMMD_ERR_TIMEOUT					0xA5

#define COMMD_USART_BAUD					4800
#define COMMD_BUFFER_SIZE					64
#define COMMD_INTLVL						USART_INT_LVL_LO
#define COMMD_TIMEOUT						(1024)

#define COMMD_TX_STATE_IDLE					0x00
#define COMMD_TX_STATE_SENDING				0x01
#define COMMD_TX_STATE_AWAIT_ANSWER			0x03
#define COMMD_TX_STATE_LISTEN				0x04
#define COMMD_TX_STATE_END_LISTEN			0x05

#define COMMD_RX_STATE_NORMAL				0x00
#define COMMD_RX_STATE_BUSERR_bm			0x01
#define COMMD_RX_STATE_DISCARD_bm			0x02
#define COMMD_RX_STATE_NOANSWER_bm			0x04
#define COMMD_RX_STATE_NOTIMEOUT_bm			0x08

typedef void (*commd_callback_t)(void* id, uint8_t answer, tba_data_t *data, uint8_t status);

typedef struct commd_struct {
	USART_t *usart;
	ring_buffer_t read_bufffer;
	ring_buffer_t write_buffer;
	uint8_t tx_state;
	uint8_t rx_state;
	uint8_t error;
	uint32_t timer;
	tba_packet_t temp;
	void *current_id;
	commd_callback_t current_callback;
} commd_t;

uint8_t commd_init(commd_t *comm, USART_t *usart);

uint8_t commd_send_cmd(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags, commd_callback_t callback, void *id);

uint8_t commd_send_data(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags, tba_data_t *data, commd_callback_t callback, void *id);

uint8_t commd_send_cmd_a(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags);

uint8_t commd_send_data_a(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags, tba_data_t *data);

uint8_t commd_update(volatile commd_t *comm);

uint8_t commd_enable_listen(commd_t *comm, commd_callback_t callback, void *id);

uint8_t commd_disable_listen(commd_t *comm);

uint8_t commd_discard_package(commd_t *comm);

void commd_timeout_handle(uint32_t time, void *data);

void commd_rxc_handle(commd_t *comm);

void commd_txc_handle(commd_t *comm);

#endif /* COMM_DRIVER_H_ */