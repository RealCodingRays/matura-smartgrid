/*
 * controller.h
 *
 * Created: 03.08.2016 23:56:06
 *  Author: Lorenzo Rai
 */ 


#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "edge.h"

#define EDGE_STATE_UNINITIALIZED		0x00
#define EDGE_STATE_XCK_HIGH				0x01
#define EDGE_STATE_AWAIT_ACK			0x02
#define EDGE_STATE_MASTER				0x03
#define EDGE_STATE_SLAVE				0x04

#define CTRL_STATE_NONE					0x00
#define CTRL_STATE_AWAIT_ACK			0x01
#define CTRL_STATE_ACK					0x02
#define CTRL_STATE_FAILED				0x03
#define CTRL_STATE_OPEN					0xA0

typedef enum ctrl_request_type_enum {
	REQ_SWAP_MASTER,
	REQ_INIT_PER,
	REQ_GET_DEVICE,
	REQ_UPDATE_DEVICE,
	REQ_SET_DEVICE,
	REQ_ENDLOOP,
	REQ_UPDATE_EDGE_STATE
} ctrl_request_type_t;

typedef struct ctrl_request_struct {
	linked_list_t list;
	ctrl_request_type_t type;
	void *data;
} ctrl_request_t;

struct ctrl_per_state_struct {
	float gen_out;
	float gen_max;
	float bat_out;
	float bat_max;
	float bat_str;
	float bat_max_str;
	float con_out;
};

void ctrl_init(edge_state_t *edge);

void ctrl_start(edge_state_t *edge);

void ctrl_update(volatile edge_state_t *edge);

void ctrl_add_request(edge_state_t *edge, uint8_t type, void *data);

void ctrl_request_done(edge_state_t *edge);


void ctrl_start_init_handler(uint32_t time, void *data);

void ctrl_init_timeout_handler(uint32_t time, void *data);

void ctrl_init_comm_handler(void *address, uint8_t answer, tba_data_t *data, uint8_t status);

void ctrl_edg_comm_handler(void *id, uint8_t answer, tba_data_t *data, uint8_t status);

void ctrl_per_comm_handler(void *id, uint8_t answer, tba_data_t *data, uint8_t status);


void ctrl_add_device(edge_state_t *edge, tba_data_t *data);

void ctrl_update_device(edge_state_t *edge, tba_data_t *data);


void ctrl_swap_master_timer(uint32_t time, void *data);

void ctrl_swap_master(edge_state_t *edge);


void ctrl_req_update_dev_iterator(endpoint_t *ep, void *data);

void ctrl_req_init_per(edge_state_t *edge);

void ctrl_req_get_device(edge_state_t *edge, endpoint_t *ep);

void ctrl_req_update_edge_state(edge_state_t *edge);

void ctrl_req_set_device(edge_state_t *edge, endpoint_t *ep);

void ctrl_req_endloop_timer(uint32_t time, void *data);

void ctrl_req_endloop(edge_state_t *edge);

#endif /* CONTROLLER_H_ */