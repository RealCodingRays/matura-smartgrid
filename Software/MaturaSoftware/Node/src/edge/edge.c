/*
 * edge.c
 *
 * Created: 28.07.2016 20:34:43
 *  Author: Lorenzo Rai
 */ 

 #include <util/atomic.h>
 #include <asf.h>
 #include <stdlib.h>
 #include "edeg_local.h"
 #include "edge.h"
 #include "comm_driver.h"
 #include "edge_board.h"
 #include "controller.h"

 edge_state_t *edge_init(const struct edge_config_struct *cfg) {
	edge_state_t *edge = malloc(sizeof(edge_state_t));
	edge->cfg = cfg;
	edge->enabled = 0;

	ll_init(&(edge->endpoints));

	commd_init(&(edge->edg_comm), cfg->edg_usart);
	commd_init(&(edge->per_comm), cfg->per_usart);

	ep_init(&(edge->endpoints));

	edgb_init(edge);

	ctrl_init(edge);

	hist_init(&(edge->history));

	return edge;
 }

 void edge_start(edge_state_t *edge, uint8_t enable) {
	edge->enabled = enable;
	if(edge->enabled) {
		ctrl_start(edge);
	}
 }

 void edge_update(edge_state_t *edge) {
	if(edge->enabled) {
		commd_update(&(edge->edg_comm));
		commd_update(&(edge->per_comm));

		ctrl_update(edge);
	}
 }

 void edge_enable(edge_state_t *edge) {
	edge->enabled = 1;
 }

 void edge_disable(edge_state_t *edge) {
	edge->enabled = 0;
 }


 const struct edge_config_struct edge1_cfg = {
	 .edg_usart = &BOARD_E1_EDG_USART,
	 .per_usart = &BOARD_E1_PER_USART,
	 .timer0 = &TCC0,
	 .timer1 = &TCC1,
	 
	 .edg_rdx = BOARD_E1_EDG_RDX,
	 .edg_tdx = BOARD_E1_EDG_TDX,
	 .edg_xck = BOARD_E1_EDG_XCK,

	 .per_rdx = BOARD_E1_PER_RDX,
	 .per_tdx = BOARD_E1_PER_TDX,
	 .per_xck = BOARD_E1_PER_XCK,

	 .open = BOARD_E1_OPEN,
	 .close = BOARD_E1_CLOSE,

	 .out_ctrla = BOARD_E1_OUTCTRLA,
	 .out_ctrlb = BOARD_E1_OUTCTRLB,
	 .in_ctrla = BOARD_E1_INCTRLA,
	 .in_ctrlb = BOARD_E1_INCTRLB,

	 .sled_red = BOARD_E1_SLED_RED,
	 .sled_green = BOARD_E1_SLED_GREEN,
	 .vled_red = BOARD_E1_VLED_RED,
	 .vled_green = BOARD_E1_VLED_GREEN
 };

 const struct edge_config_struct edge2_cfg = {
	 .edg_usart = &BOARD_E2_EDG_USART,
	 .per_usart = &BOARD_E2_PER_USART,
	 .timer0 = &TCD0,
	 .timer1 = &TCD1,
	 
	 .edg_rdx = BOARD_E2_EDG_RDX,
	 .edg_tdx = BOARD_E2_EDG_TDX,
	 .edg_xck = BOARD_E2_EDG_XCK,

	 .per_rdx = BOARD_E2_PER_RDX,
	 .per_tdx = BOARD_E2_PER_TDX,
	 .per_xck = BOARD_E2_PER_XCK,

	 .open = BOARD_E2_OPEN,
	 .close = BOARD_E2_CLOSE,

	 .out_ctrla = BOARD_E2_OUTCTRLA,
	 .out_ctrlb = BOARD_E2_OUTCTRLB,
	 .in_ctrla = BOARD_E2_INCTRLA,
	 .in_ctrlb = BOARD_E2_INCTRLB,

	 .sled_red = BOARD_E2_SLED_RED,
	 .sled_green = BOARD_E2_SLED_GREEN,
	 .vled_red = BOARD_E2_VLED_RED,
	 .vled_green = BOARD_E2_VLED_GREEN
 };

 const struct edge_config_struct edge3_cfg = {
	 .edg_usart = &BOARD_E3_EDG_USART,
	 .per_usart = &BOARD_E3_PER_USART,
	 .timer0 = &TCE0,
	 .timer1 = &TCE1,
	 
	 .edg_rdx = BOARD_E3_EDG_RDX,
	 .edg_tdx = BOARD_E3_EDG_TDX,
	 .edg_xck = BOARD_E3_EDG_XCK,

	 .per_rdx = BOARD_E3_PER_RDX,
	 .per_tdx = BOARD_E3_PER_TDX,
	 .per_xck = BOARD_E3_PER_XCK,

	 .open = BOARD_E3_OPEN,
	 .close = BOARD_E3_CLOSE,

	 .out_ctrla = BOARD_E3_OUTCTRLA,
	 .out_ctrlb = BOARD_E3_OUTCTRLB,
	 .in_ctrla = BOARD_E3_INCTRLA,
	 .in_ctrlb = BOARD_E3_INCTRLB,

	 .sled_red = BOARD_E3_SLED_RED,
	 .sled_green = BOARD_E3_SLED_GREEN,
	 .vled_red = BOARD_E3_VLED_RED,
	 .vled_green = BOARD_E3_VLED_GREEN
 };