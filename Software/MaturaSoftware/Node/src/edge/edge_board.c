/*
 * edge_board.c
 *
 * Created: 03.08.2016 13:06:28
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <inttypes.h>
 #include "edge_board.h"
 #include "edge.h"

 void edgb_init(edge_state_t *edge) {
	edge->ctrl_state = 0;
	edge->led_state = 0;
	edgb_close(edge);
	edgb_set_in(edge, EDGB_REG_OPEN);
	edgb_set_out(edge, EDGB_REG_OPEN);

	ioport_set_pin_dir(edge->cfg->sled_green, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(edge->cfg->sled_red, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(edge->cfg->vled_green, IOPORT_DIR_OUTPUT);
	ioport_set_pin_dir(edge->cfg->vled_red, IOPORT_DIR_OUTPUT);
 }

 void edgb_close(edge_state_t *edge) {
	
 }

 void edgb_open(edge_state_t *edge) {
	
 }

 void edgb_set_in(edge_state_t *edge, uint8_t stage) {
	edge->ctrl_state = (edge->ctrl_state & ~EDGB_INCTRL_gm) | (EDGB_INCTRL_gm & (stage <<  EDGB_INCTRL_gp));
	uint8_t pin0 = (stage & 0x1) > 0;
	uint8_t pin1 = (stage & 0x2) > 0;
	ioport_set_pin_level(edge->cfg->in_ctrla, pin0);
	ioport_set_pin_level(edge->cfg->in_ctrlb, pin1);
 }

 void edgb_set_out(edge_state_t *edge, uint8_t stage) {
	 edge->ctrl_state = (edge->ctrl_state & ~EDGB_OUTCTRL_gm) | (EDGB_OUTCTRL_gm & (stage <<  EDGB_OUTCTRL_gp));
	 uint8_t pin0 = (stage & 0x1) > 0;
	 uint8_t pin1 = (stage & 0x2) > 0;
	 ioport_set_pin_level(edge->cfg->out_ctrla, pin0);
	 ioport_set_pin_level(edge->cfg->out_ctrlb, pin1);
 }

 void edgb_set_led(edge_state_t *edge, uint8_t led) {
	ioport_set_pin_level(edge->cfg->sled_green, led);
 }