/*
 * edeg_local.h
 *
 * Created: 03.08.2016 12:49:54
 *  Author: Lorenzo Rai
 */ 


#ifndef EDEG_LOCAL_H_
#define EDEG_LOCAL_H_

#include "edge.h"

void edge_recalculate_endpoints(edge_state_t *edge);



#endif /* EDEG_LOCAL_H_ */