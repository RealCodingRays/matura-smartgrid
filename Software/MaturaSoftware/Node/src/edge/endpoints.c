/*
 * endpoints.c
 *
 * Created: 08.08.2016 15:27:16
 *  Author: Lorenzo Rai
 */ 

 #include <common/error.h>
 #include <stdlib.h>
 #include "endpoints.h"

 void ep_init(endpoints_t *eps) {
	ll_init(&(eps->active));
	ll_init(&(eps->inactive));
 }

 endpoint_t *ep_add(endpoints_t *eps, uint16_t uid, uint8_t bid, endpoint_state_t state, endpoint_type_t type) {
	endpoint_t *ep = malloc(sizeof(endpoint_t));
	ep->uid = uid;
	ep->bid = bid;
	ep->state = state;
	ep->type = type;
	switch(state) {
		case EP_STATE_ACTIVE:
			ll_add_before(&(eps->active), &(ep->list));
			break;
		case EP_STATE_INACTIVE:
			ll_add_before(&(eps->inactive), &(ep->list));
			break;
		default:
			free(ep);
			error_no = ERROR_BAD_ARGUMENT;
			return NULL;
			break;
	}
	return ep;
 }

 void ep_remove(endpoints_t *eps, uint8_t bid) {
	endpoint_t *ep = ep_get_bid_f(eps, bid, EP_FLAG_INCLUDE_INACTIVE);
	if(ep != NULL) {
		ll_remove(&(ep->list));
		free(ep);
	}
 }

 void ep_deactivate(endpoints_t *eps, uint8_t bid) {
	ep_set_state(eps, bid, EP_STATE_INACTIVE);
 }

 void ep_activate(endpoints_t *eps, uint8_t bid) {
	ep_set_state(eps, bid, EP_STATE_ACTIVE);
 }

 endpoint_t *ep_get_bid(endpoints_t *eps, uint8_t bid) {
	return ep_get_bid_f(eps, bid, 0);
 }

 endpoint_t *ep_get_bid_f(endpoints_t *eps, uint8_t bid, uint8_t flags) {
	linked_list_t *elem = eps->active.next;
	while(elem != &(eps->active)) {
		if(ll_get_elem(elem, endpoint_t, list)->bid == bid) {
			return ll_get_elem(elem, endpoint_t, list);
		}
		elem = elem->next;
	}
	if(flags & EP_FLAG_INCLUDE_INACTIVE) {
		elem = eps->inactive.next;
		while(elem != &(eps->inactive)) {
			if(ll_get_elem(elem, endpoint_t, list)->bid == bid) {
				return ll_get_elem(elem, endpoint_t, list);
			}
			elem = elem->next;
		}
	}
	return NULL;
 }

 endpoint_t *ep_get_uid(endpoints_t *eps, uint16_t uid) {
	return ep_get_uid_f(eps, uid, 0);
 }

 endpoint_t *ep_get_uid_f(endpoints_t *eps, uint16_t uid, uint8_t flags) {
	 linked_list_t *elem = eps->active.next;
	 while(elem != &(eps->active)) {
		 if(ll_get_elem(elem, endpoint_t, list)->uid == uid) {
			 return ll_get_elem(elem, endpoint_t, list);
		 }
		 elem = elem->next;
	 }
	 if(flags & EP_FLAG_INCLUDE_INACTIVE) {
		 elem = eps->inactive.next;
		 while(elem != &(eps->inactive)) {
			 if(ll_get_elem(elem, endpoint_t, list)->uid == uid) {
				 return ll_get_elem(elem, endpoint_t, list);
			 }
			 elem = elem->next;
		 }
	 }
	 return NULL;
 }

 void ep_foreach_type(endpoints_t *eps, endpoint_type_t type, ep_interator_t iterator, void *data) {
	ep_foreach_type_f(eps, type, iterator, data, 0);
 }

 void ep_foreach_type_f(endpoints_t *eps, endpoint_type_t type, ep_interator_t iterator, void *data, uint8_t flags) {
	 linked_list_t *elem = eps->active.next;
	 while(elem != &(eps->active)) {
		 iterator(ll_get_elem(elem, endpoint_t, list), data);
		 elem = elem->next;
	 }
	 if(flags & EP_FLAG_INCLUDE_INACTIVE) {
		 elem = eps->inactive.next;
		 while(elem != &(eps->inactive)) {
			 iterator(ll_get_elem(elem, endpoint_t, list), data);
			 elem = elem->next;
		 }
	 }
 }

 void ep_foreach_state(endpoints_t *eps, endpoint_state_t state, ep_interator_t iterator, void *data) {
	linked_list_t *head;
	switch(state) {
		case EP_STATE_ACTIVE:
			head = &(eps->active);
			break;
		case EP_STATE_INACTIVE:
			head = &(eps->inactive);
			break;
		default:
			error_no = ERROR_BAD_ARGUMENT;
			return;
	}
	linked_list_t *elem = head->next;
	while(elem != head) {
		iterator(ll_get_elem(elem, endpoint_t, list), data);
		elem = elem->next;
	}
 }

 void ep_set_state(endpoints_t *eps, uint8_t bid, endpoint_state_t state) {
	endpoint_t *ep = ep_get_bid_f(eps, bid, EP_FLAG_INCLUDE_INACTIVE);
	if(ep == NULL) {
		error_no = ERROR_BAD_ARGUMENT;
		return;
	}
	ll_remove(&(ep->list));
	switch(state) {
		case EP_STATE_ACTIVE:
			ll_add_before(&(eps->active), &(ep->list));
			break;
		case EP_STATE_INACTIVE:
			ll_add_before(&(eps->inactive), &(ep->list));
			break;
		default:
			error_no = ERROR_BAD_ARGUMENT;
			free(ep); // Avoid memory leak
			break;
	}
 }