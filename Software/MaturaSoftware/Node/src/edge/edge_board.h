/*
 * edg_board.h
 *
 * Created: 03.08.2016 13:03:05
 *  Author: Lorenzo Rai
 */ 


#ifndef EDGE_BOARD_H_
#define EDGE_BOARD_H_

#include "edge.h"
#include <inttypes.h>

#define EDGB_RELAY_bm			0b00000001
#define EDGB_INCTRL_gm			0b00000110
#define EDGB_INCTRL_gp			1
#define EDGB_OUTCTRL_gm			0b00011000
#define EDGB_OUTCTRL_gp			3

#define EDGB_REG_OPEN			0
#define EDGB_REG_S1				1
#define EDGB_REG_S2				2
#define EDGB_REG_CLOSED			3

void edgb_init(edge_state_t *edge);

void edgb_open(edge_state_t *edge);

void edgb_close(edge_state_t *edge);

void edgb_set_in(edge_state_t *edge, uint8_t stage);

void edgb_set_out(edge_state_t *edge, uint8_t stage);

void edgb_set_led(edge_state_t *edge, uint8_t led);

#endif /* EDG_BOARD_H_ */