/*
 * controller.c
 *
 * Created: 03.08.2016 23:57:15
 *  Author: Lorenzo Rai
 */ 

 #include <common/error.h>
 #include <util/atomic.h>
 #include <stdlib.h>
 #include <timer.h>
 #include <daytime.h>
 #include "edge.h"
 #include "controller.h"
 #include "protocol.h"
 #include "edge_board.h"
 #include "logger.h"

 void ctrl_init(edge_state_t *edge) {
	edge->edge_state = EDGE_STATE_UNINITIALIZED;
	edge->max_addresses = 5;
	edge->next_address = 1;

	ll_init(&(edge->requests));

	ioport_configure_pin(edge->cfg->edg_xck, IOPORT_WIRED_OR);
	ioport_set_pin_dir(edge->cfg->edg_xck, IOPORT_DIR_OUTPUT);

	edge->temp = 0;
	edge->get_state = 0;
 }

 void ctrl_start(edge_state_t *edge) {
	edge->timer = timer_add_timer((rand() / (RAND_MAX / COMMD_TIMEOUT)) + COMMD_TIMEOUT, ctrl_start_init_handler, edge);
 }

 void ctrl_update(volatile edge_state_t *edge) {
	if(edge->edge_state == EDGE_STATE_UNINITIALIZED) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			if(ioport_get_pin_level(edge->cfg->edg_xck) && edge->edge_state == EDGE_STATE_UNINITIALIZED) {
				commd_send_cmd(&(edge->edg_comm), TBA_DEFAULT_ADDRESS, PROT_START_INIT, 0, ctrl_init_comm_handler, edge);
				edge->edge_state = EDGE_STATE_AWAIT_ACK;
			}
		}
	} else {

		// Handle requests
		if(!ll_is_empty(&(edge->requests))) {
			switch(ll_get_elem(edge->requests.next, ctrl_request_t, list)->type) {
				case REQ_INIT_PER:
					ctrl_req_init_per(edge);
					break;
				case REQ_GET_DEVICE:
					ctrl_req_get_device(edge, (ll_get_elem(edge->requests.next, ctrl_request_t, list)->data));
					break;
				case REQ_UPDATE_EDGE_STATE:
					ctrl_req_update_edge_state(edge);
					break;
				case REQ_SET_DEVICE:
					ctrl_req_set_device(edge, (ll_get_elem(edge->requests.next, ctrl_request_t, list)->data));
					break;
				case REQ_ENDLOOP:
					ctrl_req_endloop(edge);
					break;
				default:
					ctrl_request_done(edge);
					break;
			}
		}
	}
 }

 void ctrl_add_request(edge_state_t *edge, ctrl_request_type_t type, void *data) {
	struct ctrl_request_struct *req = malloc(sizeof(struct ctrl_request_struct));
	if(req == NULL) {
		error_no = ERROR_MALLOC_FAILED;
		return;
	}
	req->data = data;
	req->type = type;
	ll_add_before(&(edge->requests), &(req->list));
 }

 void ctrl_request_done(edge_state_t *edge) {
	ctrl_request_t *req = ll_get_elem(edge->requests.next, ctrl_request_t, list);
	ll_remove(&(req->list));
	free(req);
	edge->temp = CTRL_STATE_NONE;
 }


 void ctrl_start_init_handler(uint32_t time, void *data) {
	edge_state_t *edge = (edge_state_t *) data;
	if((ioport_get_pin_level(edge->cfg->edg_xck) == 0) && (edge->edge_state == EDGE_STATE_UNINITIALIZED)) {
		commd_enable_listen(&(edge->edg_comm), ctrl_init_comm_handler, edge);
		ioport_set_pin_level(edge->cfg->edg_xck, 1);
		edge->timer = timer_add_timer(COMMD_TIMEOUT, ctrl_init_timeout_handler, edge);
		edge->edge_state = EDGE_STATE_XCK_HIGH;
	}
 }

 void ctrl_init_timeout_handler(uint32_t time, void *data) {
	edge_state_t *edge = (edge_state_t *) data;

	if((edge->edge_state == EDGE_STATE_UNINITIALIZED) || (edge->edge_state == EDGE_STATE_XCK_HIGH)) {
		ioport_set_pin_level(edge->cfg->edg_xck, 0);
		commd_discard_package(&(edge->edg_comm));
		edge->timer = timer_add_timer((rand() / (RAND_MAX / COMMD_TIMEOUT)) + COMMD_TIMEOUT, ctrl_start_init_handler, edge);
		edge->edge_state = EDGE_STATE_UNINITIALIZED;
	}
 }

 void ctrl_init_comm_handler(void *id, uint8_t answer, tba_data_t *data, uint8_t status) {
	volatile edge_state_t *edge = id;

	switch(status) {
		case COMMD_SUCCESS:
			
			if((answer == TBA_ACK) && (edge->edge_state == EDGE_STATE_AWAIT_ACK)) {

				edge->edge_state = EDGE_STATE_MASTER;
				ctrl_add_request(edge, REQ_INIT_PER, NULL);
				edgb_set_led(edge, 1);
			} else if((answer == PROT_START_INIT) && (edge->edge_state == EDGE_STATE_XCK_HIGH)) {
				
				ioport_set_pin_level(edge->cfg->edg_xck, 0);
				// Send ACK answer
				if(commd_send_cmd_a(&(edge->edg_comm), TBA_DEFAULT_ADDRESS, TBA_ACK, 0)) {
					edge->edge_state = EDGE_STATE_UNINITIALIZED;
					edge->timer = timer_add_timer((rand() / (RAND_MAX / COMMD_TIMEOUT)) + COMMD_TIMEOUT, ctrl_start_init_handler, edge);
					return;
				}
				edge->edge_state = EDGE_STATE_SLAVE;
				commd_discard_package(&(edge->per_comm));
				commd_enable_listen(&(edge->per_comm), ctrl_per_comm_handler, edge);
				commd_discard_package(&(edge->edg_comm));
				commd_enable_listen(&(edge->edg_comm), ctrl_edg_comm_handler, edge);
				edgb_set_led(edge, 1);
			}
			break;
		default:
			log_msg(0x05, "Edge init failed");
			if(edge->ctrl_state == EDGE_STATE_UNINITIALIZED) {
			}
			break;
	}
 }

 void ctrl_edg_comm_handler(void *id, uint8_t answer, tba_data_t *data, uint8_t status) {
	volatile edge_state_t *edge = (edge_state_t *) id;
	
 }

 void ctrl_per_comm_handler(void *id, uint8_t answer, tba_data_t *data, uint8_t status) {
	volatile edge_state_t *edge = (edge_state_t *) id;
	
	switch(status) {
		case COMMD_SUCCESS:
			switch(answer) {
				case TBA_ACK:
					if(edge->temp == CTRL_STATE_AWAIT_ACK) {
						edge->temp = CTRL_STATE_ACK;
					}
					break;
				case TBA_EP_ADD_DEVICE:
					ctrl_add_device(edge, data);
					if(edge->edge_state == EDGE_STATE_MASTER) {
						edge->temp = CTRL_STATE_ACK;
					}
					break;
				case TBA_EP_UPDATE_STATE:
					ctrl_update_device(edge, data);
					if(edge->edge_state == EDGE_STATE_MASTER) {
						edge->temp = CTRL_STATE_ACK;
					}
					break;
			}
			break;
		default:
			if(edge->edge_state == EDGE_STATE_MASTER) {
				edge->temp = CTRL_STATE_FAILED;
			} else {
				
			}
			break;
	}
 }

 
 void ctrl_add_device(edge_state_t *edge, tba_data_t *data) {
	log_msg(0x05, "Add device");
	error_no = 0;
	endpoint_t *ep = ep_get_uid_f(&(edge->endpoints), tba_get_uint16(tba_get_array(data, 1)), EP_FLAG_INCLUDE_INACTIVE);

	ioport_set_pin_level(edge->cfg->sled_red, 1);

	if(ep != NULL) {
		ep_remove(&(edge->endpoints), ep->bid);
	}

	ep = ep_add(&(edge->endpoints), tba_get_uint16(tba_get_array(data, 1)), tba_get_uint8(tba_get_array(data, 0)), EP_STATE_ACTIVE, (endpoint_type_t) tba_get_uint8(tba_get_array(data, 2)));

	ep->min_out = tba_get_float(tba_get_array(data, 3));
	ep->max_out = tba_get_float(tba_get_array(data, 4));
	ep->change = tba_get_float(tba_get_array(data, 5));
	if(ep->type == EP_TYPE_BATTERY) {
		ep->max_str = tba_get_float(tba_get_array(data, 6));
	}
 }

 void ctrl_update_device(edge_state_t *edge, tba_data_t *data) {
	log_msg(0x05, "Update device");
	error_no = 0;
	uint8_t busid = tba_get_uint8(tba_get_array(data, 0));
	endpoint_t *ep = ep_get_bid(&(edge->endpoints), busid);
	if(ep != NULL) {
		ep->cur_out = tba_get_float(tba_get_array(data, 1));
		if(ep->type == TBA_EP_TYPE_BATTERY) {
			ep->cur_str = tba_get_float(tba_get_array(data, 2));
		}
	} else {
		log_msg(0x05, "Update invalid device");
	}
 }



 void ctrl_swap_master_timer(uint32_t time, void *data) {
 }

 void ctrl_swap_master(edge_state_t *edge) {
 }



 void ctrl_req_update_dev_iterator(endpoint_t *ep, void *data) {
	ctrl_add_request((edge_state_t *) data, REQ_GET_DEVICE, ep);
 }

 void ctrl_req_init_per(edge_state_t *edge) {
	if(edge->next_address > edge->max_addresses) {
		ctrl_request_done(edge);
		return;
	}
	switch(edge->temp) {
		case CTRL_STATE_NONE: {
			tba_data_t *data = tba_create_data();
			tba_init_uint8(data, edge->next_address);
			commd_send_data(&(edge->per_comm), edge->next_address, TBA_EP_INIT, COMMD_FLAG_NOANSWER, data, ctrl_per_comm_handler, edge);
			edge->temp = CTRL_STATE_AWAIT_ACK;
			} break;
		case CTRL_STATE_AWAIT_ACK:
			break;
		case CTRL_STATE_FAILED:
		case CTRL_STATE_ACK:
			edge->next_address++;
		default:
			if(edge->next_address <= edge->max_addresses) {
				ctrl_add_request(edge, REQ_INIT_PER, NULL);
			} else {
				ep_foreach_state(&(edge->endpoints), EP_STATE_ACTIVE, ctrl_req_update_dev_iterator, edge);
				ctrl_add_request(edge, REQ_UPDATE_EDGE_STATE, NULL);
			}
			ctrl_request_done(edge);
	}
 }

 void ctrl_req_get_device(edge_state_t *edge, endpoint_t *ep) {
	switch(edge->temp) {
		case CTRL_STATE_NONE:
			log_msg(0x05, "Start get device");
			commd_send_cmd(&(edge->per_comm), ep->bid, TBA_EP_GET_STATE, 0, ctrl_per_comm_handler, edge);
			edge->temp = CTRL_STATE_AWAIT_ACK;
			break;
		case CTRL_STATE_FAILED:
			ep->cur_out = 0;
			ep->cur_str = 0;
			ep_deactivate(&(edge->endpoints), ep->bid);
			log_msg(0x05, "Get device failed");
		case CTRL_STATE_ACK:
		default:
			ctrl_request_done(edge);
	}
 }

 void ctrl_req_update_edge_state_get(endpoint_t *ep, void *data) {
	struct ctrl_per_state_struct *state = (struct ctrl_per_state_struct*) data;
	switch(ep->type) {
		case EP_TYPE_GENERATOR:
			if(ep->max_out > 0) {
				state->gen_out += ep->cur_out;
				state->gen_max += ep->max_out;
			} else {
				state->con_out += ep->cur_out;
			}
			break;
		case EP_TYPE_BATTERY:
			state->bat_out += ep->cur_out;
			state->bat_max += ep->max_out;
			state->bat_str += ep->cur_str;
			state->bat_max_str += ep->max_str;
			break;
		case EP_TYPE_CONSUMER:
			state->con_out += ep->cur_out;
			break;
	}
 }

 void ctrl_req_update_edge_state_set_gen(endpoint_t *ep, void *data) {
	struct ctrl_per_state_struct *state = (struct ctrl_per_state_struct*) data;
	ep->cur_out = (ep->max_out / state->gen_max) * (state->gen_out);
 }

 void ctrl_req_update_edge_state_set_bat(endpoint_t *ep, void *data) {
	 struct ctrl_per_state_struct *state = (struct ctrl_per_state_struct*) data;
	 ep->cur_out = (ep->max_out / state->bat_max) * (state->bat_out);
 }

 void ctrl_req_update_edge_state_upd_gen(endpoint_t *ep, void *data) {
	ctrl_add_request((edge_state_t *) data, REQ_SET_DEVICE, NULL);
 }

 void ctrl_req_update_edge_state(edge_state_t *edge) {
	ioport_set_pin_level(edge->cfg->vled_green, 1);
	struct ctrl_per_state_struct state;
	state.gen_out = 0;
	state.gen_max = 0;
	state.bat_out = 0;
	state.bat_max = 0;
	state.bat_str = 0;
	state.bat_max_str = 0;
	state.con_out = 0;
	
	ep_foreach_state(&(edge->endpoints), EP_STATE_ACTIVE, ctrl_req_update_edge_state_get, &state);

	hist_handle_data(&(edge->history), state.con_out);

	float trg_power = -state.con_out;
	float trg_bat = 0;

	float pred = hist_get_prediction(&(edge->history));
	if(pred > trg_power) {
		trg_bat -= trg_power - pred;
		trg_power = pred;
	}

	if(trg_power > (state.gen_out + (edge->history.max_change * dtime_timer_interval))) {
		trg_bat = trg_power - (state.gen_out + (edge->history.max_change * dtime_timer_interval));
	}


	state.gen_out = trg_power;
	state.bat_out = trg_bat;
	ep_foreach_type(&(edge->endpoints), EP_TYPE_GENERATOR, ctrl_req_update_edge_state_set_gen, &state);
	
	ep_foreach_type(&(edge->endpoints), EP_TYPE_BATTERY, ctrl_req_update_edge_state_set_bat, &state);

	ep_foreach_type(&(edge->endpoints), EP_TYPE_GENERATOR, ctrl_req_update_edge_state_upd_gen, edge);
	
	ep_foreach_type(&(edge->endpoints), EP_TYPE_GENERATOR, ctrl_req_update_edge_state_upd_gen, edge);

	

	ep_foreach_state(&(edge->endpoints), EP_STATE_ACTIVE, ctrl_req_update_dev_iterator, edge);
	timer_add_timer(6000, ctrl_req_endloop_timer, edge);
	ctrl_request_done(edge);
 }

 void ctrl_req_set_device(edge_state_t *edge, endpoint_t *ep) {
	switch(edge->temp) {
		case CTRL_STATE_NONE: {
			tba_data_t *data = tba_create_data();
			tba_init_float(data, ep->cur_out);
			commd_send_data(&(edge->per_comm), ep->bid, TBA_EP_UPDATE_STATE, 0, data, ctrl_per_comm_handler, edge);
			edge->temp = CTRL_STATE_AWAIT_ACK;
			} break;
		case CTRL_STATE_AWAIT_ACK:
			break;
		case CTRL_STATE_FAILED:
			ep->cur_out = 0;
			ep->cur_str = 0;
			ep_deactivate(&(edge->endpoints), ep->bid);
		case CTRL_STATE_ACK:
		default:
			ctrl_request_done(edge);
	}
 }

 void ctrl_req_endloop_timer(uint32_t time, void *data) {
	edge_state_t *edge = (edge_state_t *) data;
	if(edge->get_state) {
		ctrl_add_request(edge, REQ_UPDATE_EDGE_STATE, NULL);
		edge->get_state = 0;
	} else {
		edge->get_state = 1;
	}
 }

 void ctrl_req_endloop(edge_state_t *edge) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(edge->get_state) {
			ctrl_add_request(edge, REQ_UPDATE_EDGE_STATE, NULL);
			edge->get_state = 0;
		} else {
			edge->get_state = 1;
		}
	}
 }