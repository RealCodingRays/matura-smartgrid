/*
 * comm_driver.c
 *
 * Created: 03.08.2016 19:24:28
 *  Author: Lorenzo Rai
 */ 
 
 #include <util/atomic.h>
 #include <avr/io.h>
 #include <asf.h>
 #include <timer.h>
 #include <common/error.h>
 #include "comm_driver.h"
 #include <logger.h>

 volatile commd_t *commd_uc0 = NULL;
 volatile commd_t *commd_uc1 = NULL;
 volatile commd_t *commd_ud0 = NULL;
 volatile commd_t *commd_ud1 = NULL;
 volatile commd_t *commd_ue0 = NULL;
 volatile commd_t *commd_ue1 = NULL;
 volatile commd_t *commd_uf0 = NULL;
 volatile commd_t *commd_uf1 = NULL;

 uint8_t commd_init(commd_t *comm, USART_t *usart) {
	// Initialize usart, timer, state and error values
	comm->usart = usart;
	comm->timer = 0;
	comm->tx_state = COMMD_TX_STATE_IDLE;
	comm->rx_state = COMMD_RX_STATE_NORMAL;
	comm->error = ERROR_SUCCESS;

	// Initialize usart module
	usart_serial_options_t cfg = {
		.baudrate = COMMD_USART_BAUD,
		.charlength = USART_CHSIZE_9BIT_gc,
		.paritytype = USART_PMODE_ODD_gc,
		.stopbits = 1
	};

	usart_serial_init(comm->usart, &cfg);

	if(comm->usart == &USARTC0) {
		commd_uc0 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTC, 3), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTC1) {
		commd_uc1 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTC, 7), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTD0) {
		commd_ud0 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 3), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTD1) {
		commd_ud1 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTD, 7), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTE0) {
		commd_ue0 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTE, 3), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTE1) {
		commd_ue1 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTE, 7), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTF0) {
		commd_uf0 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTF, 3), IOPORT_DIR_OUTPUT);
	} else if(comm->usart == &USARTF1) {
		commd_uf1 = comm;
		ioport_set_pin_dir(IOPORT_CREATE_PIN(PORTF, 7), IOPORT_DIR_OUTPUT);
	}

	// Enable interrupts
	usart_set_rx_interrupt_level(comm->usart, COMMD_INTLVL);
	usart_set_tx_interrupt_level(comm->usart, COMMD_INTLVL);

	// Initialize buffers
	rb_init(&(comm->read_bufffer), COMMD_BUFFER_SIZE);
	rb_init(&(comm->write_buffer), COMMD_BUFFER_SIZE);

	// Initialize buffer packet
	tba_init_packet(&(comm->temp));
	return COMMD_SUCCESS;
 }

 uint8_t commd_update(volatile commd_t *comm) {
	uint8_t ret = COMMD_SUCCESS;
	// If discard is set reset tmep
	if(comm->rx_state & COMMD_RX_STATE_DISCARD_bm) {
		tba_destroy_packet(&(comm->temp));
		tba_init_packet(&(comm->temp));
		comm->rx_state &= ~COMMD_RX_STATE_DISCARD_bm;
	}

	// Iterate over the read buffer
	while(rb_can_read(&(comm->read_bufffer))) {
		error_clear();
		enum tba_return_code_enum ret_h = tba_handle_data(&(comm->temp), rb_getd(&(comm->read_bufffer)));

		if(ret_h == TBA_ERROR_NO) {
			ret = error_no;
			
			// Set error flags
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				comm->rx_state |= COMMD_RX_STATE_BUSERR_bm;

				// Clean comm_t and call callback function
				comm->tx_state = COMMD_TX_STATE_IDLE;
				commd_callback_t callback = comm->current_callback;
				comm->current_callback = NULL;
				if(callback) {
					callback(comm->current_id, 0, NULL, error_no);
				}

				tba_destroy_packet(&(comm->temp));
				tba_init_packet(&(comm->temp));

				if(comm->tx_state == COMMD_TX_STATE_END_LISTEN) {
					comm->tx_state = COMMD_TX_STATE_IDLE;
				}
			}
		} else if(ret_h == TBA_END) {
			
			// Clean comm_t and call callback function
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				comm->tx_state = COMMD_TX_STATE_IDLE;
				commd_callback_t callback = comm->current_callback;
				comm->current_callback = NULL;
				if(callback) {
					callback(comm->current_id, comm->temp.cmd, comm->temp.data, COMMD_SUCCESS);
				}
				
				tba_destroy_packet(&(comm->temp));
				tba_init_packet(&(comm->temp));

				if(comm->tx_state == COMMD_TX_STATE_END_LISTEN) {
					comm->tx_state = COMMD_TX_STATE_IDLE;
				}
			}
		}
	}

	return ret;
 }

 uint8_t commd_send_cmd(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags, commd_callback_t callback, void *id) {
	return commd_send_data(comm, address, cmd, flags, tba_create_data(), callback, id);
 }

 uint8_t commd_send_data(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags, tba_data_t *data, commd_callback_t callback, void *id) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		// Check for active transmission
		if((comm->tx_state != COMMD_TX_STATE_IDLE) && (comm->tx_state != COMMD_TX_STATE_LISTEN) && (comm->tx_state != COMMD_TX_STATE_END_LISTEN)) {
			if(!(flags & COMMD_FLAG_KEEP_DATA)) {
				tba_destroy_data(data);
				free(data);
			}
			return COMMD_ERR_COMM_BUSY;
		}

		// Check for buffer overflow
		if(!rb_can_write(&(comm->write_buffer))) {
			if(!(flags & COMMD_FLAG_KEEP_DATA)) {
				tba_destroy_data(data);
				free(data);
			}
			return COMMD_ERR_BUFF_OVF;
		}

		// Save buffer state
  		rb_state_t state = rb_save_state(&(comm->write_buffer));

		// Write command
 		rb_putd(&(comm->write_buffer), cmd);

		// Write data
		error_clear();
		tba_write_data(&(comm->write_buffer), data);
		if(error_no) {

			if(!(flags & COMMD_FLAG_KEEP_DATA)) {
				tba_destroy_data(data);
				free(data);
			}
			// Restore buffer state
			rb_restore_state(&(comm->write_buffer), state);

			switch(error_no) {
				case ERROR_MALLOC_FAILED:
					return COMMD_ERR_MALLOC_FAILED;
				case ERROR_BUFFER_OVERFLOW:
					return COMMD_ERR_BUFF_OVF;
				default:
					return COMMD_ERR_UNKNOWN;
			}
		}

		// Write address and start transmission
		if(!(flags & COMMD_FLAG_NOCALLBACK)) {
			comm->current_id = id;
			comm->current_callback = callback;
			comm->rx_state &= ~COMMD_RX_STATE_NOANSWER_bm;
		} else {
			comm->rx_state |= COMMD_RX_STATE_NOANSWER_bm;
		}

		if(flags & COMMD_FLAG_NOANSWER) {
			comm->rx_state |= COMMD_RX_STATE_NOANSWER_bm;
		}

		comm->usart->CTRLB |= USART_TXB8_bm;
		comm->usart->DATA = address;
		while(!usart_data_register_is_empty(comm->usart)) {}
		comm->usart->CTRLB &= ~USART_TXB8_bm;

		if(flags & COMMD_FLAG_NOTIMEOUT) {
			comm->rx_state |= COMMD_RX_STATE_NOTIMEOUT_bm;
		} else {
			comm->rx_state &= ~COMMD_RX_STATE_NOTIMEOUT_bm;
		}
		comm->tx_state = COMMD_TX_STATE_SENDING;
	}
	return COMMD_SUCCESS;
 }

 uint8_t commd_send_cmd_a(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags) {
	return commd_send_cmd(comm, address, cmd, (flags | COMMD_FLAG_NOANSWER | COMMD_FLAG_NOTIMEOUT), NULL, NULL);
 }

 uint8_t commd_send_data_a(commd_t *comm, uint8_t address, uint8_t cmd, uint8_t flags, tba_data_t *data) {
	return commd_send_data(comm, address, cmd, (flags | COMMD_FLAG_NOANSWER | COMMD_FLAG_NOTIMEOUT), data, NULL, NULL);
 }

 uint8_t commd_enable_listen(commd_t *comm, commd_callback_t callback, void *id) {
	 ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		 // Check bus activity
		 if(comm->tx_state == COMMD_TX_STATE_IDLE) {
			 
			 comm->tx_state = COMMD_TX_STATE_LISTEN;
			 comm->current_callback = callback;
			 comm->current_id = id;
		 } else {
			 
			 return COMMD_ERR_COMM_BUSY;
		 }
	 }
	 return COMMD_SUCCESS;
 }

 uint8_t commd_disable_listen(commd_t *comm) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		// Check if state is listening
		if(comm->tx_state == COMMD_TX_STATE_LISTEN) {
			
			comm->tx_state = COMMD_TX_STATE_END_LISTEN;
		} else {
			
			return COMMD_ERR_COMM_BUSY;
		}
	}
	return COMMD_SUCCESS;
 }

 uint8_t commd_discard_package(commd_t *comm) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		// Set buserr, clean comm_t and if necessary stop timeout timer
		comm->tx_state = COMMD_TX_STATE_IDLE;
		comm->rx_state |= COMMD_RX_STATE_BUSERR_bm | COMMD_RX_STATE_DISCARD_bm;
		comm->current_id = NULL;
		comm->current_callback = NULL;
		if(comm->timer) {
			timer_stop_timer(comm->timer);
		}

		rb_clear(&(comm->read_bufffer));
	}
	return COMMD_SUCCESS;
 }

 void commd_timeout_handle(uint32_t time, void *data) {
	commd_t *comm = (commd_t *) data;

	// Clean comm_t and call callback function
	if(comm->tx_state == COMMD_TX_STATE_AWAIT_ANSWER) {
		comm->tx_state = COMMD_TX_STATE_IDLE;
		commd_callback_t callback = comm->current_callback;
		comm->current_callback = NULL;
		if(callback) {
			callback(comm->current_id, 0, NULL, COMMD_ERR_TIMEOUT);
		}
		
		rb_clear(&(comm->read_bufffer));
		comm->rx_state |= COMMD_RX_STATE_BUSERR_bm | COMMD_RX_STATE_DISCARD_bm;
	}
 }


 void commd_rxc_handle(commd_t *comm) {
	 if(comm) {
		 error_push();

		 uint8_t status = comm->usart->STATUS;

		 // Stop timeout timer
		 if(comm->timer) {
			 timer_stop_timer(comm->timer);
			 comm->timer = 0;
		 }

		 // Check for bit 8 set
		 if(comm->usart->STATUS & USART_RXB8_bm) {

			 uint8_t address = comm->usart->DATA;
			 // If valid start of frame reset busser
			 if((address == TBA_DEFAULT_ADDRESS) && ((comm->tx_state == COMMD_TX_STATE_AWAIT_ANSWER) || (comm->tx_state == COMMD_TX_STATE_LISTEN) || (comm->tx_state == COMMD_TX_STATE_END_LISTEN)) && !(status & (USART_BUFOVF_bm | USART_FERR_bm | USART_PERR_bm))) {
				 comm->rx_state &= ~COMMD_RX_STATE_BUSERR_bm;
			 } else {
				 comm->rx_state |= (COMMD_RX_STATE_BUSERR_bm & COMMD_RX_STATE_DISCARD_bm);
				 rb_clear(&(comm->read_bufffer));
			 }
		 } else {

			 uint8_t data = comm->usart->DATA;
			 // If no errors add data to the read buffer
			 if(((comm->tx_state == COMMD_TX_STATE_AWAIT_ANSWER) || (comm->tx_state == COMMD_TX_STATE_LISTEN) || (comm->tx_state == COMMD_TX_STATE_END_LISTEN)) && !(status & (USART_BUFOVF_bm | USART_FERR_bm | USART_PERR_bm))) {
				 //If busserr discard data
				 if(!(comm->rx_state & COMMD_RX_STATE_BUSERR_bm)) {
					 error_clear();
					 rb_putd(&(comm->read_bufffer), data);

					 if(error_no) {
						 comm->error = error_no;
						 comm->rx_state |= (COMMD_RX_STATE_BUSERR_bm & COMMD_RX_STATE_DISCARD_bm);
						 rb_clear(&(comm->read_bufffer));
					 }
				 }
			 } else {
				 comm->rx_state |= (COMMD_RX_STATE_BUSERR_bm & COMMD_RX_STATE_DISCARD_bm);
				 rb_clear(&(comm->read_bufffer));
			 }
		 }
		 error_pop();
	 }
 }

 void commd_txc_handle(commd_t *comm) {
	 if(comm) {
		 error_push();
		 
		 // If buffer is empty start timeout timer and set tx_state
		 if(rb_can_read(&(comm->write_buffer))) {
			 comm->usart->DATA = rb_getd(&(comm->write_buffer));
		 } else {
			 if(comm->tx_state == COMMD_TX_STATE_SENDING) {
				 if(!(comm->rx_state & COMMD_RX_STATE_NOANSWER_bm)) {
					comm->tx_state = COMMD_TX_STATE_AWAIT_ANSWER;
				 } else {
					comm->tx_state = COMMD_TX_STATE_IDLE;
				 }
				 if(!(comm->rx_state & COMMD_RX_STATE_NOTIMEOUT_bm)) {
					comm->timer = timer_add_timer(COMMD_TIMEOUT, commd_timeout_handle, comm);
					comm->tx_state = COMMD_TX_STATE_AWAIT_ANSWER;
				 }
			 }
		 }
		 error_pop();
	 }
 }

 ISR(USARTC0_RXC_vect) {
	commd_rxc_handle(commd_uc0);
 }

 ISR(USARTC0_TXC_vect) {
	 commd_txc_handle(commd_uc0);
 }

 ISR(USARTC1_RXC_vect) {
	 commd_rxc_handle(commd_uc1);
 }

 ISR(USARTC1_TXC_vect) {
	 commd_txc_handle(commd_uc1);
 }

 ISR(USARTD0_RXC_vect) {
	 commd_rxc_handle(commd_ud0);
 }

 ISR(USARTD0_TXC_vect) {
	 commd_txc_handle(commd_ud0);
 }

 ISR(USARTD1_RXC_vect) {
	 commd_rxc_handle(commd_ud1);
 }

 ISR(USARTD1_TXC_vect) {
	 commd_txc_handle(commd_ud1);
 }

 ISR(USARTE0_RXC_vect) {
	 commd_rxc_handle(commd_ue0);
 }

 ISR(USARTE0_TXC_vect) {
	 commd_txc_handle(commd_ue0);
 }

 ISR(USARTE1_RXC_vect) {
	 commd_rxc_handle(commd_ue1);
 }

 ISR(USARTE1_TXC_vect) {
	 commd_txc_handle(commd_ue1);
 }

 ISR(USARTF0_RXC_vect) {
	 commd_rxc_handle(commd_uf0);
 }

 ISR(USARTF0_TXC_vect) {
	 commd_txc_handle(commd_uf0);
 }

 ISR(USARTF1_RXC_vect) {
	 commd_rxc_handle(commd_uf1);
 }

 ISR(USARTF1_TXC_vect) {
	 commd_txc_handle(commd_uf1);
 }