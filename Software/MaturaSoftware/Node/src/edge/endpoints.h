/*
 * endpoints.h
 *
 * Created: 08.08.2016 15:03:28
 *  Author: Lorenzo Rai
 */ 


#ifndef ENDPOINTS_H_
#define ENDPOINTS_H_

#include <inttypes.h>
#include <common/linked_list.h>
#include <common/tba.h>

#define EP_FLAG_INCLUDE_INACTIVE		0b00000001

typedef struct endpoints_struct {
	linked_list_t active;
	linked_list_t inactive;
} endpoints_t;

typedef enum endpoint_state_enum {
	EP_STATE_ACTIVE,
	EP_STATE_INACTIVE
} endpoint_state_t;

typedef enum endpoint_type_enum {
	EP_TYPE_GENERATOR = TBA_EP_TYPE_GENERATOR,
	EP_TYPE_BATTERY = TBA_EP_TYPE_BATTERY,
	EP_TYPE_CONSUMER = TBA_EP_TYPE_CONSUMER
} endpoint_type_t;

typedef struct endpoint_struct {
	linked_list_t list;
	uint16_t uid;
	uint8_t bid;
	endpoint_state_t state;
	endpoint_type_t type;
	float min_out;
	float max_out;
	float cur_out;
	float change;
	float max_str;
	float cur_str;
} endpoint_t;

typedef void (*ep_interator_t)(endpoint_t *ep, void *data);

void ep_init(endpoints_t *eps);

endpoint_t *ep_add(endpoints_t *eps, uint16_t uid, uint8_t bid, endpoint_state_t state, endpoint_type_t type);

void ep_remove(endpoints_t *eps, uint8_t bid);

void ep_deactivate(endpoints_t *eps, uint8_t bid);

void ep_activate(endpoints_t *eps, uint8_t bid);

endpoint_t *ep_get_bid(endpoints_t *eps, uint8_t bid);

endpoint_t *ep_get_uid(endpoints_t *eps, uint16_t uid);

endpoint_t *ep_get_bid_f(endpoints_t *eps, uint8_t bid, uint8_t flags);

endpoint_t *ep_get_uid_f(endpoints_t *eps, uint16_t uid, uint8_t flags);

void ep_foreach_type(endpoints_t *eps, endpoint_type_t type, ep_interator_t iterator, void *data);

void ep_foreach_type_f(endpoints_t *eps, endpoint_type_t type, ep_interator_t iterator, void *data, uint8_t flags);

void ep_foreach_state(endpoints_t *eps, endpoint_state_t state, ep_interator_t iterator, void *data);

void ep_set_state(endpoints_t *eps, uint8_t bid, endpoint_state_t state);

#endif /* ENDPOINTS_H_ */