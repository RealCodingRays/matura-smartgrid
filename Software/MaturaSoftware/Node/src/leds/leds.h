/*
 * leds.h
 *
 * Created: 17.07.2016 14:41:09
 *  Author: Lorenzo Rai
 */ 


#ifndef LEDS_H_
#define LEDS_H_

#define LEDS_TIMER				TCF1
#define LEDS_TIMER_PERIOD		1000

void leds_init();



#endif /* LEDS_H_ */