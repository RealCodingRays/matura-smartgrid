/*
 * daytime.h
 *
 * Created: 12.08.2016 17:19:23
 *  Author: Lorenzo Rai
 */ 


#ifndef DAYTIME_H_
#define DAYTIME_H_

#include <inttypes.h>

#define DTIME_TIMER		TCF1
#define DTIME_PERIOD	((32000000 / 1024) / 4320)

extern uint32_t dtime_timer_interval;

void dtime_init(void);

uint32_t dtime_get_time(void);

void dtime_callib_timer(void);

static inline uint8_t dtime_get_sec(void) {
	return dtime_get_time() % 60;
}

static inline uint8_t dtime_get_min(void) {
	return (dtime_get_time() % (60 * 60)) / (60);
}

static inline uint8_t dtime_get_hour(void) {
	return (uint8_t)((dtime_get_time() % ((uint16_t) 60 * 60 * 24)) / (60 * 60));
}

static inline uint16_t dtime_get_day(void) {
	return dtime_get_time() / ((uint16_t) 60 * 60 * 24);
}

static inline uint16_t dtime_cut_day(void) {
	return dtime_get_time() % ((uint16_t) 60 * 60 * 24);
}

static inline uint16_t dtime_cut_hour(void) {
	return dtime_get_time() % (60 * 60);
}

static inline uint8_t time_get_sec(uint32_t time) {
	return time % 60;
}

static inline uint8_t time_get_min(uint32_t time) {
	return (time % (60 * 60)) / (60);
}

static inline uint8_t time_get_hour(uint32_t time) {
	return (time % ((uint16_t) 60 * 60 * 24)) / (60 * 60);
}

static inline uint16_t time_get_day(uint32_t time) {
	return (time) / ((uint16_t) 60 * 60 * 24);
}

static inline uint16_t time_cut_day(uint32_t time) {
	return time % ((uint16_t) 60 * 60 * 24);
}

static inline uint16_t time_cut_hour(uint32_t time) {
	return time % (60 * 60);
}

#endif /* DAYTIME_H_ */