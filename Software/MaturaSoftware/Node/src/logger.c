/*
 * logger.c
 *
 * Created: 05.08.2016 16:26:35
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <util/atomic.h>
 #include <common/linked_list.h>
 #include <string.h>
 #include "logger.h"

 #ifdef DEBUG
 volatile uint8_t *next_entry = (uint8_t *) 3;
 volatile uint8_t num_of_entries = 0;
 #endif

 void log_init() {
	#ifdef DEBUG
	eeprom_write_byte((uint8_t *) 2, num_of_entries);
	#endif
 }

 void log_update() {
 }

 void log_msg(uint8_t src, char *msg) {
	#ifdef DEBUG
	size_t bytes = (strlen(msg) * sizeof(char)) + 1;
	uint32_t time = rtc_get_time();
	uint8_t *dest = 0;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		dest = next_entry;
		next_entry += bytes + 5;
		num_of_entries++;
		if(next_entry < ((uint8_t *) EEPROM_SIZE)) {
			eeprom_write_byte((uint8_t *) 2, num_of_entries);
		} else {
			eeprom_write_byte((uint8_t *) 2, 0xFF);
			return;
		}
	}
	eeprom_write_byte(dest, src);
	dest++;
	eeprom_write_dword((uint32_t *) dest, time);
	dest += 4;
	eeprom_write_block(msg, dest, bytes);
	#endif
 }