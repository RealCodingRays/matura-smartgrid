/*
 * timer.h
 *
 * Created: 03.08.2016 14:01:34
 *  Author: Lorenzo Rai
 */ 


#ifndef TIMER_H_
#define TIMER_H_

#include <asf.h>
#include <common/linked_list.h>

#define TIMER_TOP			0xFFFF

typedef void (*timer_callback_t)(uint32_t time, void *data);

struct timer_event_struct {
	linked_list_t list;
	uint32_t id;
	uint32_t time;
	timer_callback_t callback;
	void *data;
};

void timer_init(void);

void timer_start(void);

uint32_t timer_add_timer(uint32_t time, timer_callback_t callback, void *data);

void timer_stop_timer(uint32_t id);

void timer_recalculate(void);

void timer_cmp_callback(uint32_t time);

void timer_ov_callback(void);


#endif /* TIMER_H_ */