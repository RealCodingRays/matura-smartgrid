/*
 * hmi.h
 *
 * Created: 28.07.2016 20:55:12
 *  Author: Lorenzo Rai
 */ 


#ifndef HMI_H_
#define HMI_H_

#include <inttypes.h>

#define HMI_DISABLED_PSU		(1)
#define HMI_DISABLED_EDGE1		(1 << 1)
#define HMI_DISABLED_EDGE2		(1 << 2)
#define HMI_DISABLED_EDGE3		(1 << 3)

void hmi_init(void);

uint8_t hmi_get_disabled(void);

uint8_t hmi_is_disabled(uint8_t mask);

#endif /* HMI_H_ */