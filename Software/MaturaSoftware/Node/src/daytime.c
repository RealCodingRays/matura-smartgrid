/*
 * daytime.c
 *
 * Created: 12.08.2016 17:59:08
 *  Author: Lorenzo Rai
 */ 


 #include <util/atomic.h>
 #include <asf.h>
 #include "daytime.h"
 #include "timer.h"

 volatile uint32_t dtime_time;
 uint32_t dtime_timer_interval;

 void dtime_ov_callback(void) {
	dtime_time ++;
 }

 void dtime_init() {
	dtime_time = 0;

	tc_set_wgm(&DTIME_TIMER, TC_WG_NORMAL);
	tc_set_overflow_interrupt_level(&DTIME_TIMER, TC_INT_LVL_LO);
	tc_set_overflow_interrupt_callback(&DTIME_TIMER, dtime_ov_callback);
	tc_write_period(&DTIME_TIMER, DTIME_PERIOD);
	tc_write_clock_source(&DTIME_TIMER, TC_CLKSEL_DIV1024_gc);
 }

 void dtime_callib_callback(uint32_t time, void *data) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		dtime_timer_interval = (dtime_time - dtime_timer_interval);
	}
 }

 void dtime_callib_timer() {
	 uint32_t time = dtime_get_time();
	dtime_timer_interval = time;
	timer_add_timer(6000, dtime_callib_callback, NULL);
	cpu_irq_enable();
	uint8_t repeat;
	do {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			repeat = (dtime_timer_interval == time);
		}
	} while(repeat);
 }

 uint32_t dtime_get_time() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		return dtime_time;
	}
 }