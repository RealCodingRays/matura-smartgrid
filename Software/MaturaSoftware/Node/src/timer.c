/*
 * timer.c
 *
 * Created: 03.08.2016 14:12:46
 *  Author: Lorenzo Rai
 */ 

 #include <util/atomic.h>
 #include <common/error.h>
 #include "timer.h"
 #include "logger.h"

 static uint32_t timer_next_id = 0;
 linked_list_t timer_events;
 struct timer_event_struct *timer_current = NULL;

 void timer_init() {
	rtc_init();
	rtc_set_callback(timer_cmp_callback);
	sysclk_disable_module(SYSCLK_PORT_GEN, SYSCLK_RTC);

	ll_init(&timer_events);
 }

 void timer_start() {
	sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_RTC);
	timer_recalculate();
 }

 uint32_t timer_add_timer(uint32_t time, timer_callback_t callback, void *data) {
	struct timer_event_struct *evt = malloc(sizeof(struct timer_event_struct));
	evt->time = rtc_get_time() + time;
	evt->data = data;
	evt->id = timer_next_id++;
	evt->callback = callback;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		linked_list_t *elem = timer_events.next;
		while(elem != &timer_events) {
			if(ll_get_elem(elem, struct timer_event_struct, list)->time > evt->time) {
				break;
			}

			elem = elem->next;
		}

		ll_add_before(elem, &(evt->list));
		timer_recalculate();
	}
	return evt->id;
 }

 void timer_stop_timer(uint32_t id) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		linked_list_t *elem = timer_events.next;
		while(elem != &timer_events) {
			if(ll_get_elem(elem, struct timer_event_struct, list)->id == id) {
				ll_remove(elem);
				free(ll_get_elem(elem, struct timer_event_struct, list));
				timer_recalculate();
				return;
			}
			elem = elem->next;
		}
	}
 }

 void timer_recalculate() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		timer_current = NULL;
		if(timer_events.next != &timer_events) {
			timer_current = ll_get_elem(timer_events.next, struct timer_event_struct, list);
		}
		if(timer_current != NULL) {
			rtc_set_alarm(timer_current->time);
		}
	}
 }

 void timer_cmp_callback(uint32_t time) {
	if(timer_current != NULL) {
		error_push();
		ll_remove(&(timer_current->list));
		struct timer_event_struct *evt = timer_current;
		evt->callback(time, evt->data);
		free(evt);

		linked_list_t *elem = timer_events.next;
		while((elem != &timer_events)) {
			uint32_t r_time = rtc_get_time();
			if(ll_get_elem(elem, struct timer_event_struct, list)->time - 10 <= r_time) {
				ll_remove(elem);
				struct timer_event_struct *evt = ll_get_elem(elem, struct timer_event_struct, list);
				evt->callback(r_time, evt->data);
				free(evt);
			} else break;
			elem = timer_events.next;
		}

		timer_recalculate();
		error_pop();
	}
 }