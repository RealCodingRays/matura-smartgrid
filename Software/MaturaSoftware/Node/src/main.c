/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>
#include "hmi.h"
#include "edge/edge.h"
#include "sensors/sensors.h"
#include "timer.h"
#include "logger.h"
#include "daytime.h"

volatile edge_state_t *edge1;
volatile edge_state_t *edge2;
volatile edge_state_t *edge3;

volatile uint8_t stuff = 0;

int main (void)
{
	sysclk_init();
	srand(eeprom_read_byte(0) | (eeprom_read_byte(1) << 8));

	board_init();

	pmic_init();
	pmic_set_scheduling(PMIC_SCH_ROUND_ROBIN);

	log_init();
	log_msg(0x00, "Start");

	dtime_init();
	timer_init();
	timer_start();
	dtime_callib_timer();
	sen_init();
	edge1 = edge_init(&edge1_cfg);
	edge2 = edge_init(&edge2_cfg);
	edge3 = edge_init(&edge3_cfg);

	sen_start();
	edge_start(edge1, /*hmi_is_disabled(HMI_DISABLED_EDGE1)*/1);
	edge_start(edge2, /*hmi_is_disabled(HMI_DISABLED_EDGE2)*/1);
	edge_start(edge3, /*hmi_is_disabled(HMI_DISABLED_EDGE3)*/1);

	cpu_irq_enable();

	while(1) {
		edge_update(edge1);
		edge_update(edge2);
		edge_update(edge3);
	}
}
