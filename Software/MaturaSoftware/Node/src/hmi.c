/*
 * hmi.c
 *
 * Created: 28.07.2016 20:58:02
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <avr/interrupt.h>
 #include "hmi.h"

 void hmi_init() {
	
 }

 uint8_t hmi_get_disabled() {
	return (ioport_get_pin_level(BOARD_PSU_DISABLE) == 1 ? HMI_DISABLED_PSU : 0) | (ioport_get_pin_level(BOARD_E1_DISABLE) == 1 ? HMI_DISABLED_EDGE1 : 0) | (ioport_get_pin_level(BOARD_E2_DISABLE) == 1 ? HMI_DISABLED_EDGE2 : 0) | (ioport_get_pin_level(BOARD_E3_DISABLE) == IOPORT_PIN_LEVEL_HIGH ? HMI_DISABLED_EDGE3 : 0);
 }

 uint8_t hmi_is_disabled(uint8_t mask) {
	return hmi_get_disabled() & mask;
 }