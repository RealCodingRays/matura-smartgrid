/*
 * sen_int.h
 *
 * Created: 17.07.2016 12:48:05
 *  Author: Lorenzo Rai
 */ 


#ifndef SEN_INT_H_
#define SEN_INT_H_

void sen_voltage_callback(ADC_t *adc, uint8_t ch_mask, adc_result_t res);

void sen_current_callback(ADC_t *adc, uint8_t ch_mask, adc_result_t res);

void sen_swap_buffers(void);

void sen_start_conversion(void);

#endif /* SEN_INT_H_ */