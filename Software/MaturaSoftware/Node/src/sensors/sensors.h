/*
 * sensors.h
 *
 * Created: 17.07.2016 12:35:01
 *  Author: Lorenzo Rai
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

#define SEN_E1_VSEN_CH				ADC_CH0
#define SEN_E2_VSEN_CH				ADC_CH1
#define SEN_E3_VSEN_CH				ADC_CH2
#define SEN_BUFFER_VSEN_CH			ADC_CH3

#define SEN_E1_ISEN_CH				ADC_CH0
#define SEN_E2_ISEN_CH				ADC_CH1
#define SEN_E3_ISEN_CH				ADC_CH2

#define SEN_CURRENT_GAIN			64
#define SEN_RES_MAX					2047
#define SEN_RES_MIN					-2047

#define SEN_VOLTAGE_CHANNELS		4
#define SEN_CURRENT_CHANNELS		3

struct sen_voltage_data_struct {
	float edge1;
	float edge2;
	float edge3;
	float buffer;
};

struct sen_current_data_struct {
	float edge1;
	float edge2;
	float edge3;
};

extern volatile struct sen_voltage_data_struct *sen_voltage_data;
extern volatile struct sen_current_data_struct *sen_current_data;

void sen_init(void);

void sen_start(void);

void sen_stop(void);

void sen_update(void);

#endif /* SENSORS_H_ */