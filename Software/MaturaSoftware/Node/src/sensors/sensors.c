/*
 * sensors.c
 *
 * Created: 17.07.2016 12:48:34
 *  Author: Lorenzo Rai
 */ 

 #include <asf.h>
 #include <util/atomic.h>
 #include "sensors.h"
 #include "sensors_local.h"

 volatile uint8_t sen_enable = 0;
 volatile uint8_t sen_v_rdy = SEN_VOLTAGE_CHANNELS;
 volatile uint8_t sen_i_rdy = SEN_CURRENT_CHANNELS;

 volatile struct sen_voltage_data_struct *sen_voltage_data;
 volatile struct sen_current_data_struct *sen_current_data;
 volatile struct sen_voltage_data_struct *sen_voltage_buffer;
 volatile struct sen_current_data_struct *sen_current_buffer;

 volatile struct sen_voltage_data_struct sen_vdata1;
 volatile struct sen_voltage_data_struct sen_vdata2;
 volatile struct sen_current_data_struct sen_idata1;
 volatile struct sen_current_data_struct sen_idata2;

 void sen_init() {
	struct adc_config adc_cfg;
	struct adc_channel_config adcch_cfg;

	//Initialize voltage adc
	adc_read_configuration(&BOARD_VOLTAGE_ADC, &adc_cfg);

	adc_set_conversion_parameters(&adc_cfg, ADC_SIGN_ON, ADC_RES_12, ADC_REF_BANDGAP);
	adc_set_conversion_trigger(&adc_cfg, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_cfg, 20000U);
	
	adc_write_configuration(&BOARD_VOLTAGE_ADC, &adc_cfg);
	adcch_read_configuration(&BOARD_VOLTAGE_ADC, ADC_CH0, &adcch_cfg);

	adcch_enable_interrupt(&adcch_cfg);

	adcch_set_input(&adcch_cfg, BOARD_E1_VSEN_ADC, BOARD_GND_ADC, 1);
	adcch_write_configuration(&BOARD_VOLTAGE_ADC, SEN_E1_VSEN_CH, &adcch_cfg);
	
	adcch_set_input(&adcch_cfg, BOARD_E2_VSEN_ADC, BOARD_GND_ADC, 1);
	adcch_write_configuration(&BOARD_VOLTAGE_ADC, SEN_E2_VSEN_CH, &adcch_cfg);
	
	adcch_set_input(&adcch_cfg, BOARD_E3_VSEN_ADC, BOARD_GND_ADC, 1);
	adcch_write_configuration(&BOARD_VOLTAGE_ADC, SEN_E3_VSEN_CH, &adcch_cfg);
	
	adcch_set_input(&adcch_cfg, BOARD_BUFFER_VSEN_ADC, BOARD_GND_ADC, 1);
	adcch_write_configuration(&BOARD_VOLTAGE_ADC, SEN_BUFFER_VSEN_CH, &adcch_cfg);

	//Initialize current adc
	adc_read_configuration(&BOARD_CURRENT_ADC, &adc_cfg);

	adc_set_conversion_parameters(&adc_cfg, ADC_SIGN_ON, ADC_RES_12, ADC_REF_BANDGAP);
	adc_set_conversion_trigger(&adc_cfg, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_cfg, 20000U);
	
	adc_write_configuration(&BOARD_CURRENT_ADC, &adc_cfg);
	adcch_read_configuration(&BOARD_CURRENT_ADC, ADC_CH0, &adcch_cfg);

	adcch_enable_interrupt(&adcch_cfg);

	adcch_set_input(&adcch_cfg, BOARD_E1_ISENIN_ADC, BOARD_E1_ISENOUT_ADC, SEN_CURRENT_GAIN);
	adcch_write_configuration(&BOARD_CURRENT_ADC, SEN_E1_ISEN_CH, &adcch_cfg);
	
	adcch_set_input(&adcch_cfg, BOARD_E2_ISENIN_ADC, BOARD_E2_ISENOUT_ADC, SEN_CURRENT_GAIN);
	adcch_write_configuration(&BOARD_CURRENT_ADC, SEN_E2_ISEN_CH, &adcch_cfg);
	
	adcch_set_input(&adcch_cfg, BOARD_E3_ISENIN_ADC, BOARD_E3_ISENOUT_ADC, SEN_CURRENT_GAIN);
	adcch_write_configuration(&BOARD_CURRENT_ADC, SEN_E3_ISEN_CH, &adcch_cfg);

	adc_set_callback(&BOARD_VOLTAGE_ADC, &sen_voltage_callback);
	adc_set_callback(&BOARD_CURRENT_ADC, &sen_current_callback);

	adc_enable(&BOARD_VOLTAGE_ADC);
	adc_enable(&BOARD_CURRENT_ADC);

	//Initialize buffers
	sen_vdata1.edge1 = 0;
	sen_vdata1.edge2 = 0;
	sen_vdata1.edge3 = 0;
	sen_vdata1.buffer = 0;

	sen_idata1.edge1 = 0;
	sen_idata1.edge2 = 0;
	sen_idata1.edge3 = 0;

	sen_voltage_data = &sen_vdata1;
	sen_voltage_buffer = &sen_vdata2;
	sen_current_data = &sen_idata1;
	sen_current_buffer = &sen_idata2;
 }

 void sen_start() {
	sen_enable = 1;
	sen_start_conversion();
 }

 void sen_stop() {
	sen_enable = 0;
 }

 void sen_voltage_callback(ADC_t *adc, uint8_t ch_mask, adc_result_t res) {
	float voltage = (((float) res)/((float) SEN_RES_MAX))/BOARD_VDIV_FACTOR;
	switch(ch_mask) {
		case SEN_E1_VSEN_CH:
			sen_voltage_buffer->edge1 = voltage;
			break;
		case SEN_E2_VSEN_CH:
			sen_voltage_buffer->edge2 = voltage;
			break;
		case SEN_E3_VSEN_CH:
			sen_voltage_buffer->edge3 = voltage;
			break;
		case SEN_BUFFER_VSEN_CH:
			sen_voltage_buffer->buffer = voltage;
			break;
	}
	sen_v_rdy--;
	if(sen_v_rdy <= 0 && sen_i_rdy <= 0) {
		sen_swap_buffers();
		sen_start_conversion();
	}
 }

 void sen_current_callback(ADC_t *adc, uint8_t ch_mask, adc_result_t res) {
	float current = (((((float) res)/((float) SEN_RES_MAX))/((float) SEN_CURRENT_GAIN))/BOARD_VDIV_FACTOR)/BOARD_ISEN_R;
	switch(ch_mask) {
		case SEN_E1_ISEN_CH:
			sen_current_buffer->edge1 = current;
			break;
		case SEN_E2_ISEN_CH:
			sen_current_buffer->edge2 = current;
			break;
		case SEN_E3_ISEN_CH:
			sen_current_buffer->edge3 = current;
			break;
	}
	sen_i_rdy--;
	if(sen_v_rdy <= 0 && sen_i_rdy <= 0) {
		sen_swap_buffers();
		sen_start_conversion();
	}
 }

 void sen_swap_buffers() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		struct sen_voltage_data_struct *vtemp = sen_voltage_data;
		sen_voltage_data = sen_voltage_buffer;
		sen_voltage_buffer = vtemp;

		struct sen_current_data_struct *itemp = sen_current_data;
		sen_current_data = sen_current_buffer;
		sen_current_buffer = itemp;
	}
 }

 void sen_start_conversion() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(sen_enable) {
			sen_v_rdy = SEN_VOLTAGE_CHANNELS;
			sen_i_rdy = SEN_CURRENT_CHANNELS;
			adc_start_conversion(&BOARD_VOLTAGE_ADC, SEN_E1_VSEN_CH | SEN_E2_VSEN_CH | SEN_E3_VSEN_CH | SEN_BUFFER_VSEN_CH);
			adc_start_conversion(&BOARD_CURRENT_ADC, SEN_E1_ISEN_CH | SEN_E2_ISEN_CH | SEN_E3_ISEN_CH);
		}
	}
 }