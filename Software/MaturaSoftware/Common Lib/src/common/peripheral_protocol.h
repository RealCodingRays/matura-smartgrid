/*
 * peripheral_protocol.h
 *
 * Created: 29.07.2016 13:02:01
 *  Author: Lorenzo Rai
 */ 


#ifndef PERIPHERAL_PROTOCOL_H_
#define PERIPHERAL_PROTOCOL_H_

#define PER_CMD_READ_DEVICES			0xA5
#define PER_CMD_ADD_DEVICE				0xA6


#endif /* PERIPHERAL_PROTOCOL_H_ */