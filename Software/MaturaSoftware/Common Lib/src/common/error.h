/*
 * error.h
 *
 * Created: 03.08.2016 19:46:45
 *  Author: Lorenzo Rai
 */ 


#ifndef ERROR_H_
#define ERROR_H_

#include <inttypes.h>

#define ERROR_SUCCESS				0x00

#define ERROR_MALLOC_FAILED			0x01
#define ERROR_BUFFER_OVERFLOW		0x02
#define ERROR_BUFFER_UNDERFLOW		0x03
#define ERROR_INVALID_POINTER		0x04
#define ERROR_BAD_ARGUMENT			0x05

extern uint8_t error_no;

uint8_t error_push(void);

uint8_t error_pop(void);

void error_clear(void);

#endif /* ERROR_H_ */