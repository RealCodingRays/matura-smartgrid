/*
 * ring_buffer.h
 *
 * Created: 24.07.2016 13:12:47
 *  Author: Lorenzo Rai
 */ 


#ifndef RING_BUFFER_H_
#define RING_BUFFER_H_

#include <stddef.h>
#include <inttypes.h>
#include "error.h"

typedef struct ring_buffer_struct {
	uint8_t *data;
	uint8_t *rptr;
	uint8_t *wptr;
	size_t size;
} ring_buffer_t;

typedef struct rb_state_struct {
	uint8_t *rptr;
	uint8_t *wptr;
} rb_state_t;

void rb_init(ring_buffer_t *buffer, size_t size);

void rb_destroy(ring_buffer_t *buffer);

uint8_t rb_can_read(ring_buffer_t *buffer);

uint8_t rb_can_write(ring_buffer_t *buffer);

void rb_put(ring_buffer_t *buffer, uint8_t *data);

void rb_putd(ring_buffer_t *buffer, uint8_t data);

void rb_get(ring_buffer_t *buffer, uint8_t *data);

uint8_t rb_getd(ring_buffer_t *buffer);

void rb_clear(ring_buffer_t *buffer);

rb_state_t rb_save_state(ring_buffer_t *buffer);

void rb_restore_state(ring_buffer_t *buffer, rb_state_t state);

#endif /* RING_BUFFER_H_ */