/*
 * linked_list.c
 *
 * Created: 29.07.2016 12:08:05
 *  Author: Lorenzo Rai
 */ 

 #include "linked_list.h"

 void ll_init(linked_list_t *list) {
	list->next = list;
	list->prev = list;
 }

 void ll_add(linked_list_t *list, linked_list_t *elem) {
	elem->next = list->next;
	elem->prev = list->prev;
	list->next->prev = elem;
	list->next = elem;
 }

 void ll_add_before(linked_list_t *list, linked_list_t *elem) {
	elem->next = list;
	elem->prev = list->prev;
	list->prev->next = elem;
	list->prev = elem;
 }

 void ll_remove(linked_list_t *elem) {
	elem->next->prev = elem->prev;
	elem->prev->next = elem->next;
 }

 uint8_t ll_is_empty(linked_list_t *list) {
	return list->next == list;
 }