/*
 * tba.h
 *
 * Created: 12.07.2016 17:31:34
 *  Author: Lorenzo Rai
 */ 


#ifndef TBA_H_
#define TBA_H_

#include <stddef.h>
#include <inttypes.h>
#include "ring_buffer.h"

#define TBA_TYPE_VOID			0x00
#define TBA_TYPE_ARRAY			0x01
#define TBA_TYPE_NUMBER			0x02
#define TBA_TYPE_STRING			0x03

#define TBA_ACK					0x01
#define TBA_NACK				0x02

#define TBA_EP_PRE_INIT			0x03
#define TBA_EP_INIT				0x04
#define TBA_EP_ADD_DEVICE		0x05
#define TBA_EP_SET_ADDRESS		0x06
#define TBA_EP_GET_STATE		0x07
#define TBA_EP_UPDATE_STATE		0x08

#define TBA_EP_TYPE_CONSUMER	0xA7
#define TBA_EP_TYPE_BATTERY		0x8E
#define TBA_EP_TYPE_GENERATOR	0x72

#define TBA_DEFAULT_ADDRESS		0x00

enum tba_return_code_enum {
	TBA_END, TBA_NONE, TBA_ERROR_NO
};

typedef struct tba_data_struct {
	size_t bytes;
	uint8_t type;
	size_t size;
	size_t index;
	void *data;
} tba_data_t;

typedef struct tba_packet_struct {
	uint16_t byte;
	uint8_t cmd;
	tba_data_t *data;
} tba_packet_t;

typedef union tba_float_union {
	float f;
	uint8_t b[4];
} tba_float_t;

tba_packet_t *tba_create_packet(void);

void tba_init_packet(tba_packet_t *packet);

tba_data_t *tba_create_data(void);

void tba_destroy_packet(tba_packet_t *packet);

void tba_destroy_data(tba_data_t *data);

enum tba_return_code_enum tba_handle_data(tba_packet_t *packet, uint8_t data);

enum tba_return_code_enum tba_handle_data_pkg(tba_packet_t *packet, tba_data_t *pkg, uint8_t data);

void tba_write_packet(ring_buffer_t *buffer, uint8_t cmd, tba_data_t *pkg);

void tba_write_data(ring_buffer_t *buffer, tba_data_t *pkg);

size_t tba_write_size(tba_data_t *pkg);

void tba_init_void(tba_data_t *data);

void tba_init_array(tba_data_t *data, size_t size);

void tba_init_uint8(tba_data_t *data, uint8_t num);

void tba_init_uint16(tba_data_t *data, uint16_t num);

void tba_init_uint32(tba_data_t *data, uint32_t num);

void tba_init_float(tba_data_t *data, float num);

uint8_t tba_get_type(tba_data_t *data);

size_t tba_get_size(tba_data_t *data);

tba_data_t *tba_get_array(tba_data_t *data, size_t index);

uint8_t tba_get_uint8(tba_data_t *data);

uint16_t tba_get_uint16(tba_data_t *data);

uint32_t tba_get_uint32(tba_data_t *data);

float tba_get_float(tba_data_t *data);

#endif /* TBA_H_ */