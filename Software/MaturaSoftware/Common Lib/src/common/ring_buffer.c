/*
 * ring_buffer.c
 *
 * Created: 24.07.2016 13:20:12
 *  Author: Lorenzo Rai
 */ 

 #include <stdlib.h>
 #include <util/atomic.h>
 #include "ring_buffer.h"


 void rb_init(ring_buffer_t *buffer, size_t size) {
	buffer->data = malloc(size);
	buffer->rptr = buffer->data;
	buffer->wptr = buffer->data;
	buffer->size = size;
 }

 void rb_destroy(ring_buffer_t *buffer) {
	free(buffer->data);
 }

 uint8_t rb_can_read(ring_buffer_t *buffer) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		return buffer->rptr != buffer->wptr;
	}
 }

 uint8_t rb_can_write(ring_buffer_t *buffer) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		return ((buffer->wptr + 1) >= (buffer->data + buffer->size)) ? (buffer->rptr != buffer->data) : ((buffer->wptr + 1) != buffer->rptr);
	}
 }

 void rb_put(ring_buffer_t *buffer, uint8_t *data) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(rb_can_write(buffer)) {
			*(buffer->wptr) = *data;
			buffer->wptr ++;
			if(buffer->wptr >= (buffer->data + buffer->size)) {
				buffer->wptr = buffer->data;
			}
		} else {
			error_no = ERROR_BUFFER_OVERFLOW;
		}
	}
 }

 void rb_putd(ring_buffer_t *buffer, uint8_t data) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(rb_can_write(buffer)) {
			*(buffer->wptr) = data;
			buffer->wptr ++;
			if(buffer->wptr >= (buffer->data + buffer->size)) {
				buffer->wptr = buffer->data;
			}
		} else {
			error_no = ERROR_BUFFER_OVERFLOW;
		}
	}
 }

 void rb_get(ring_buffer_t *buffer, uint8_t *data) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(rb_can_read(buffer)) {
			*data = *(buffer->rptr);
			buffer->rptr++;
			if(buffer->rptr >= (buffer->data + buffer->size)) {
				buffer->rptr = buffer->data;
			}
		} else {
			error_no = ERROR_BUFFER_UNDERFLOW;
		}
	}
 }

 uint8_t rb_getd(ring_buffer_t *buffer) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(rb_can_read(buffer)) {
			uint8_t ret = *(buffer->rptr);
			buffer->rptr++;
			if(buffer->rptr >= (buffer->data + buffer->size)) {
				buffer->rptr = buffer->data;
			}
			return ret;
		} else {
			error_no = ERROR_BUFFER_UNDERFLOW;
			return 0;
		}
	}
 }

 void rb_clear(ring_buffer_t *buffer) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		buffer->rptr = buffer->wptr;
	}
 }

 rb_state_t rb_save_state(ring_buffer_t *buffer) {
	rb_state_t state;
	state.rptr = buffer->rptr;
	state.wptr = buffer->wptr;
	return state;
 }

 void rb_restore_state(ring_buffer_t *buffer, rb_state_t state) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		buffer->rptr = state.rptr;
		buffer->wptr = state.wptr;
	}
 }