/*
 * linked_list.h
 *
 * Created: 29.07.2016 12:07:54
 *  Author: Lorenzo Rai
 */ 


#ifndef LINKED_LIST_H_
#define LINKED_LIST_H_

#include <inttypes.h>

typedef struct linked_list_struct {
	struct linked_list_struct *next;
	struct linked_list_struct *prev;
} linked_list_t;

#define ll_get_elem(list, type, member)		((type *) (((uint8_t *) list) - offsetof(type, member)))

void ll_init(linked_list_t *list);

void ll_add(linked_list_t *list, linked_list_t *elem);

void ll_add_before(linked_list_t *list, linked_list_t *elem);

void ll_remove(linked_list_t *elem);

uint8_t ll_is_empty(linked_list_t *list);

#endif /* LINKED_LIST_H_ */