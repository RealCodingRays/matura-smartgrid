/*
 * error.c
 *
 * Created: 03.08.2016 19:49:09
 *  Author: Lorenzo Rai
 */ 

 #include <stdlib.h>
 #include <util/atomic.h>
 #include "error.h"

 struct error_no_struct {
	uint8_t error_no;
	struct error_no_struct *prev;
 };

 uint8_t error_no;
 struct error_no_struct *error_stack = NULL;

 uint8_t error_push(void) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		struct error_no_struct *new = malloc(sizeof(struct error_no_struct));
		if(new == NULL) {
			return ERROR_MALLOC_FAILED;
		}

		new->prev = error_stack;
		new->error_no = error_no;
		error_stack = new;
		error_no = 0;
	}
	return ERROR_SUCCESS;
 }

 uint8_t error_pop(void) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		struct error_no_struct *prev = error_stack->prev;
		if(prev == NULL) {
			return ERROR_BUFFER_UNDERFLOW;
		}

		free(error_stack);
		error_stack = prev;
		error_no = error_stack->error_no;
	}
	return ERROR_SUCCESS;
 }

 void error_clear() {
	error_no = ERROR_SUCCESS;
 }