/*
 * tba.c
 *
 * Created: 29.07.2016 14:39:11
 *  Author: Lorenzo Rai
 */ 

 #include <stdlib.h>
 #include "tba.h"

 tba_packet_t *tba_create_packet() {
	tba_packet_t *ret = malloc(sizeof(tba_packet_t));
	if(ret == NULL) {
		error_no = ERROR_MALLOC_FAILED;
		return NULL;
	}

	tba_init_packet(ret);
	return ret;
 }

 void tba_init_packet(tba_packet_t *packet) {
	packet->byte = 0;
	packet->data = NULL;
	packet->cmd = 0;
 }

 tba_data_t *tba_create_data() {
	tba_data_t *ret = malloc(sizeof(tba_data_t));
	if(ret == NULL) {
		error_no = ERROR_MALLOC_FAILED;
		return NULL;
	}

	tba_init_void(ret);
	return ret;
 }

 void tba_destroy_packet(tba_packet_t *packet) {
	if(packet->data != NULL) {
		tba_destroy_data(packet->data);
		free(packet->data);
	} else {
		error_no = ERROR_BAD_ARGUMENT;
	}
 }

 void tba_destroy_data(tba_data_t *data) {
	if(data->data != NULL) {
		if(data->type == TBA_TYPE_ARRAY) {
			for(size_t i = 0; i < data->size; i++) {
				tba_destroy_data(&(((tba_data_t *) data->data)[i]));
			}
		}
		free(data->data);
	} else {
		error_no = ERROR_BAD_ARGUMENT;
	}
 }

 enum tba_return_code_enum tba_handle_data(tba_packet_t *packet, uint8_t data) {
	enum tba_return_code_enum ret = TBA_NONE;
	if(packet->byte == 0) {
		packet->cmd = data;
	} else {
		if(packet->byte == 1) {
			error_clear();
			packet->data = tba_create_data();
			if(error_no) {
				return TBA_ERROR_NO;
			}
		}
		enum tba_return_code_enum retc = tba_handle_data_pkg(packet, packet->data, data);
		if(retc == TBA_ERROR_NO) {
			return TBA_ERROR_NO;
		} else if(retc == TBA_END) {
			ret = TBA_END;
		}
	}
	packet->byte ++;
	return ret;
 }

 enum tba_return_code_enum tba_handle_data_pkg(tba_packet_t *packet, tba_data_t *pkg, uint8_t data) {
	switch(pkg->bytes) {
		case 0:
			pkg->type = data;
			break;
		case 1:
			pkg->size = data;
			break;
		case 2:
			pkg->size |= (data << 8);
			break;
		default:
			switch(pkg->type) {
				case TBA_TYPE_ARRAY: {
					enum tba_return_code_enum retc = tba_handle_data_pkg(packet, &(((tba_data_t *) pkg->data)[pkg->index]), data);
					if(retc == TBA_ERROR_NO) {
						return TBA_ERROR_NO;
					} else if(retc == TBA_END) {
						pkg->index ++;
					}}
					break;
				case TBA_TYPE_NUMBER:
				case TBA_TYPE_STRING:
					((uint8_t *) pkg->data)[pkg->bytes - 3] = data;
					pkg->index++;
					break;
			}
	};
	pkg->bytes++;
	if(pkg->bytes >= 3 && pkg->index == pkg->size) {
		return TBA_END;
	}
	if(pkg->bytes == 2 && pkg->type != TBA_TYPE_VOID) {
		pkg->data = malloc((pkg->type == TBA_TYPE_ARRAY) ? (pkg->size * sizeof(tba_data_t)) : pkg->size);
		if(pkg->data == NULL) {
			error_no = ERROR_MALLOC_FAILED;
			return TBA_ERROR_NO;
		}
		if(pkg->type == TBA_TYPE_ARRAY) {
			for(size_t i = 0; i < pkg->size; i++) {
				tba_init_void(&(((tba_data_t *) pkg->data)[i]));
			}
		}
	}
	return TBA_NONE;
 };

 void tba_write_packet(ring_buffer_t *buffer, uint8_t cmd, tba_data_t *pkg) {
	rb_putd(buffer, cmd);
	tba_write_data(buffer, pkg);
 }

 void tba_write_data(ring_buffer_t *buffer, tba_data_t *pkg) {
	if(pkg == NULL || buffer == NULL) {
		error_no = ERROR_BAD_ARGUMENT;
		return;
	}
	rb_putd(buffer, pkg->type);
	rb_putd(buffer, (uint8_t) (pkg->size));
	rb_putd(buffer, (uint8_t) (pkg->size >> 8));
	if(pkg->data != NULL) {
		switch(pkg->type) {
			case TBA_TYPE_ARRAY:
				for(size_t i = 0; i < pkg->size; i++) {
					tba_write_data(buffer, &(((tba_data_t*) pkg->data)[i]));
				}
				break;
			case TBA_TYPE_NUMBER:
			case TBA_TYPE_STRING:
				for(size_t i = 0; i < pkg->size; i++) {
					rb_putd(buffer, ((uint8_t *) pkg->data)[i]);
				}
				break;
		}
	}
 }

 void tba_init_void(tba_data_t *data) {
	data->data = NULL;
	data->size = 0;
	data->type = TBA_TYPE_VOID;
	data->bytes = 0;
	data->index = 0;
 }

 void tba_init_uint8(tba_data_t *data, uint8_t num) {
	data->type = TBA_TYPE_NUMBER;
	data->size = 1;
	data->data = malloc(1);
	*((uint8_t *) data->data) = num;
 }

 void tba_init_uint16(tba_data_t *data, uint16_t num) {
	data->type = TBA_TYPE_NUMBER;
	data->size = 2;
	data->data = malloc(2);
	*((uint8_t *) data->data) = ((uint8_t) num);
	*((uint8_t *) (data->data + 1)) = ((uint8_t) (num >> 8));
 }

 void tba_init_uint32(tba_data_t *data, uint32_t num) {
	data->type = TBA_TYPE_NUMBER;
	data->size = 4;
	data->data = malloc(4);
	*((uint8_t *) data->data) = ((uint8_t) num);
	*((uint8_t *) (data->data + 1)) = ((uint8_t) (num >> 8));
	*((uint8_t *) (data->data + 2)) = ((uint8_t) (num >> 16));
	*((uint8_t *) (data->data + 3)) = ((uint8_t) (num >> 24));
 }

 void tba_init_float(tba_data_t *data, float num) {
	data->type = TBA_TYPE_NUMBER;
	data->size = 4;
	data->data = malloc(4);
	tba_float_t val;
	val.f = num;
	for(int i = 0; i < 4; i++) {
		((uint8_t *) data->data)[i] = val.b[i];
	}
 }

 void tba_init_array(tba_data_t *data, size_t size) {
	data->type = TBA_TYPE_ARRAY;
	data->size = size;
	data->data = malloc(size * sizeof(tba_data_t));
	for(size_t i = 0; i < size; i++) {
		tba_init_void(&(((tba_data_t *) data->data)[i]));
	}
 }

 uint8_t tba_get_type(tba_data_t *data) {
	return data->type;
 }

 size_t tba_get_size(tba_data_t *data) {
	return data->size;
 }

 tba_data_t *tba_get_array(tba_data_t *data, size_t index) {
	if(data->type == TBA_TYPE_ARRAY && data->data != NULL) {
		return &(((tba_data_t *) data->data)[index]);
	}
	return NULL;
 }

 uint8_t tba_get_uint8(tba_data_t *data) {
	return *((uint8_t *) data->data);
 }

 uint16_t tba_get_uint16(tba_data_t *data) {
	uint16_t ret = 0;
	ret |= *((uint8_t *) data->data);
	ret |= ((uint16_t) *((uint8_t *) (data->data + 1))) << 8;
	return ret;
 }

 uint32_t tba_get_uint32(tba_data_t * data) {
	uint32_t ret = 0;
	ret |= *((uint8_t *) data->data);
	ret |= ((uint32_t) (*((uint8_t *) (data->data + 1)))) << 8;
	ret |= ((uint32_t) (*((uint8_t *) (data->data + 1)))) << 16;
	ret |= ((uint32_t) (*((uint8_t *) (data->data + 1)))) << 24;
	return ret;
 }

 float tba_get_float(tba_data_t *data) {
	tba_float_t val;
	for(int i = 0; i < 4; i++) {
		val.b[i] = ((uint8_t *) data->data)[i];
	}
	return val.f;
 }