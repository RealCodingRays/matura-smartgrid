/*
 * include.h
 *
 * Created: 26.06.2016 17:17:50
 *  Author: Lorenzo Rai
 */ 


#ifndef INCLUDE_H_
#define INCLUDE_H_

#include <avr/io.h>
#include <util/atomic.h>
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "system/System.h"

#endif /* INCLUDE_H_ */