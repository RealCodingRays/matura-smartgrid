/*
 * Node.c
 *
 * Created: 25.06.2016 09:58:32
 * Author : Lorenzo Rai
 */ 

#include <string.h>
#include "include.h"
#include "system/Boot.h"
#include "system/drivers/RTC.h"
#include <util/LinkedList.h>


volatile uint8_t timers[4];
volatile char log_msgs[1024] = "Start \n";
volatile uint8_t done = 0;
volatile uint8_t done2 = 0;
volatile uint8_t cnt = 0;

void main_rtc_callback(uint8_t id) {
	char msg[128];
	sprintf(msg, "-> Callback at %i with id %i\n", rtc_get_time(), id);
	strcat(log_msgs, msg);
	cnt++;
	if(cnt == 4) {
		done = 1;
	}
	if(cnt > 4) {
		done2 = 1;
	}
}

int main(void)
{
	sys_boot();

	timers[0] = rtc_add_timer(100, &main_rtc_callback);
	timers[1] = rtc_add_timer(200, &main_rtc_callback);
	timers[2] = rtc_add_timer(50, &main_rtc_callback);
	timers[3] = rtc_add_timer(1000, &main_rtc_callback);

	sei();

    while (1) 
    {
		if(done == 1) {
			printf(log_msgs);
			done = 0;
		}
		if(done2 == 1) {
			printf("FAILED");
			sys_crash("Error in rtc");
		}
    }
	sys_crash("Main loop left execution");
}

