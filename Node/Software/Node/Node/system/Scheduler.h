/*
 * Scheduler.h
 *
 * Created: 25.06.2016 09:59:17
 *  Author: Lorenzo Rai
 */ 


#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "System.h"

void sched_task_end();

void sched_init();

struct task_struct *sched_get_task(uint8_t id);

struct task_struct *sched_find_next_task();

extern void native_do_switch(void **old_sp, void **new_sp);

extern void native_dead_switch(void **new_sp);

extern void native_create_stack(void **sp, task_t task, task_t return_addres);

#endif /* SCHEDULER_H_ */