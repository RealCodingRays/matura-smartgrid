/*
 * Error.c
 *
 * Created: 26.06.2016 17:34:34
 *  Author: Lorenzo Rai
 */ 

 #include <avr/interrupt.h>
 #include <avr/io.h>
 #include "Error.h"
 #include "System.h"
 #include "drivers/PMIC.h"

 void sys_crash(char *msg) {
	cli();
	pmic_disable_all();
	//error_enter_safe_state();
	//error_init_error_mode();
	//error_write_error(msg);
	cli();
	CCP = CCP_IOREG_gc;
	RST.CTRL = RST_SWRST_bm;
 }