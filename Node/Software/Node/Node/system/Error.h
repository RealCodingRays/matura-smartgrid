/*
 * Error.h
 *
 * Created: 26.06.2016 17:36:26
 *  Author: Lorenzo Rai
 */ 


#ifndef ERROR_H_
#define ERROR_H_

#include <stddef.h>

void error_enter_safe_state();

void error_init_error_mode();

void error_write_error(char *msg);

#endif /* ERROR_H_ */