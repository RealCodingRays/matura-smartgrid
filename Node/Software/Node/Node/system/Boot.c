/*
 * Boot.c
 *
 * Created: 26.06.2016 17:31:53
 *  Author: Lorenzo Rai
 */ 

 #include "Boot.h"
 #include "drivers/CLK.h"
 #include "drivers/RTC.h"
 #include "drivers/PMIC.h"
 #include "Scheduler.h"

 void sys_boot() {
	clk_init();
	rtc_init();
	sched_init();
	pmic_init();
 }