/*
 * Boot.h
 *
 * Created: 26.06.2016 17:31:45
 *  Author: Lorenzo Rai
 */ 


#ifndef BOOT_H_
#define BOOT_H_

void sys_boot();

#endif /* BOOT_H_ */