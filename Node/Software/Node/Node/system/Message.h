/*
 * Message.h
 *
 * Created: 25.06.2016 10:16:45
 *  Author: Lorenzo Rai
 */ 


#ifndef MESSAGE_H_
#define MESSAGE_H_

#include <inttypes.h>


typedef struct message_struct {
	uint8_t source;
	uint8_t msg;
	void *data;
} message_t;


message_t *msg_get();

void msg_send(uint8_t target, uint8_t msg, void* data);

#endif /* MESSAGE_H_ */