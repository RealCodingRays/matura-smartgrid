/*
 * TC.h
 *
 * Created: 25.06.2016 10:32:30
 *  Author: Lorenzo Rai
 */ 


#ifndef TC_H_
#define TC_H_

#include <inttypes.h>
#include <avr/io.h>


static inline void tc1_set_clk(volatile TC1_t tc, TC_CLKSEL_t clk) {
	tc.CTRLA = (tc.CTRLA & (~TC0_CLKSEL_gm)) | clk;
}

static inline void tc1_set_top(volatile TC1_t tc, uint16_t top) {
	tc.PER = top;
}

#endif /* TC_H_ */