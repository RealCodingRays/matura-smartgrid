/*
 * RTC.c
 *
 * Created: 02.07.2016 08:38:23
 *  Author: Lorenzo Rai
 */ 

 #include <util/atomic.h>
 #include <avr/interrupt.h>
 #include <stddef.h>
 #include <stdlib.h>
 #include "RTC.h"
 #include "TC.h"
 #include "../util/IDs.h"
 #include "LinkedList.h"

 struct rtc_timer_struct {
	linked_list_t list;
	uint8_t id;
	rtc_callback_t callback;
	uint32_t time;
 };

 volatile ids32_t rtc_ids;
 volatile linked_list_t rtc_timers_head;
 volatile uint8_t rtc_current;


 void rtc_init() {
	ids32_create(&rtc_ids);
	ll_create(&rtc_timers_head);

	RTC.PER = 0xFFFF;

	EVSYS.CH0MUX = EVSYS_CHMUX_RTC_OVF_gc;

	tc1_set_clk(RTC_OV_TC, TC_CLKSEL_EVCH0_gc);
	tc1_set_top(RTC_OV_TC, 0xFFFF);

	RTC.CTRL = RTC_PRESCALER_DIV1_gc;
 }

 uint32_t rtc_get_time() {
	return ((uint32_t) RTC.CNT) | (((uint32_t) RTC_OV_TC.CNT) << 16);
 }

 struct rtc_timer_struct *rtc_get_timer(uint8_t id) {
	linked_list_t *elem = rtc_timers_head.next;
	while((elem != &rtc_timers_head) && (ll_get_elem(elem, struct rtc_timer_struct, list)->id != id)) {
		elem = elem->next;
	}
	if(elem == &rtc_timers_head) {
		return NULL;
	}
	return ll_get_elem(elem, struct rtc_timer_struct, list);
 }

 uint8_t rtc_add_timer(uint32_t time, rtc_callback_t callback) {
	if(time > 10) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			struct rtc_timer_struct *timer = malloc(sizeof(struct rtc_timer_struct));
			timer->callback = callback;
			timer->id = ids32_aquire(&rtc_ids);
			if(timer->id == 0) {
				return 0;
			}
			time += rtc_get_time();
			timer->time = time;
			if(time < RTC.COMP || RTC.COMP == 0) {
				RTC.COMP = time;
				rtc_current = timer->id;
				RTC.INTCTRL |= RTC_COMPINTLVL_LO_gc;
			}
			ll_add_before(&rtc_timers_head, &(timer->list));
			return timer->id;
		}
	}
	return 0;
 }

 void rtc_update_comp() {
	linked_list_t *elem = rtc_timers_head.next;
	while(elem != &rtc_timers_head) {
		if(RTC.COMP > ll_get_elem(elem, struct rtc_timer_struct, list)->time) {
			RTC.COMP = ll_get_elem(elem, struct rtc_timer_struct, list)->time;
			rtc_current = ll_get_elem(elem, struct rtc_timer_struct, list)->id;
		}
		elem = elem->next;
	}
 } 	

 void rtc_stop_timer(uint8_t id) {
	struct rtc_timer_struct *timer = rtc_get_timer(id);
	if(timer == NULL) {
		return;
	}
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		ll_remove(&(timer->list));
		if(rtc_timers_head.next == &rtc_timers_head) {
			RTC.INTCTRL &= ~(RTC_COMPINTLVL_gm);
			RTC.COMP = 0;
			rtc_current = 0;
		} else if(RTC.COMP == timer->time) {
			rtc_update_comp();
		}
		free(timer);
	}
 }

 ISR(RTC_COMP_vect) {
	struct rtc_timer_struct *timer = rtc_get_timer(rtc_current);
	if(timer == NULL) {
		rtc_update_comp();
	} else {
		timer->callback(rtc_current);
		rtc_stop_timer(rtc_current);
	}
 }