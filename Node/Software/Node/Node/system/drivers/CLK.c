/*
 * CLK.c
 *
 * Created: 02.07.2016 08:21:55
 *  Author: Lorenzo Rai
 */ 

 #include "CLK.h"

 void clk_init() {
	OSC.CTRL = CLK_ACTIVE_OSC;
	while((OSC.STATUS & CLK_ACTIVE_OSC) != CLK_ACTIVE_OSC) {}
	
	CCP = CCP_IOREG_gc;
	CLK.CTRL = CLK_SYSCLK;

	CLK.RTCCTRL = CLK_RTCCLK | CLK_RTCEN_bm;
 }