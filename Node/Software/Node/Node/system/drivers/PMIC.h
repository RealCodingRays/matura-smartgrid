/*
 * PMIC.h
 *
 * Created: 26.06.2016 17:38:18
 *  Author: Lorenzo Rai
 */ 


#ifndef PMIC_H_
#define PMIC_H_

#include <avr/io.h>


static inline void pmic_init() {
	PMIC.CTRL = PMIC_RREN_bm | PMIC_LOLVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_HILVLEN_bm;
}

static inline void pmic_disable_all() {
	PMIC.CTRL &= PMIC_RREN_bm;
	asm volatile("nop\nnop\n");
}

#endif /* PMIC_H_ */