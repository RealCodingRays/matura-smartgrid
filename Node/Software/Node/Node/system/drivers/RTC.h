/*
 * RTC.h
 *
 * Created: 25.06.2016 10:32:06
 *  Author: Lorenzo Rai
 */ 


#ifndef RTC_H_
#define RTC_H_

#include <inttypes.h>
#include <avr/io.h>

#define		RTC_OV_TC		TCC1

typedef void (*rtc_callback_t)(uint8_t id);


uint32_t rtc_get_time();

uint8_t rtc_add_timer(uint32_t time, rtc_callback_t callback);

void rtc_stop_timer(uint8_t id);

void rtc_init();

#endif /* RTC_H_ */