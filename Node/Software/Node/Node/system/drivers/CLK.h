/*
 * CLK.h
 *
 * Created: 02.07.2016 08:21:36
 *  Author: Lorenzo Rai
 */ 


#ifndef CLK_H_
#define CLK_H_

#include <avr/io.h>

#define		CLK_ACTIVE_OSC		(OSC_RC32MEN_bm | OSC_RC32KEN_bm)
#define		CLK_SYSCLK			(CLK_SCLKSEL_RC32M_gc)
#define		CLK_RTCCLK			(CLK_RTCSRC_RCOSC_gc)


void clk_init();

#endif /* CLK_H_ */