/*
 * DMAC.h
 *
 * Created: 25.06.2016 10:32:38
 *  Author: Lorenzo Rai
 */ 


#ifndef DMAC_H_
#define DMAC_H_

#include <inttypes.h>


uint8_t dmac_add_transfer_request();


#endif /* DMAC_H_ */