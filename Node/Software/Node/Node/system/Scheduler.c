/*
 * Scheduler.c
 *
 * Created: 25.06.2016 10:37:30
 *  Author: Lorenzo Rai
 */ 

 #include <stddef.h>
 #include <stdlib.h>
 #include <util/atomic.h>
 #include "Scheduler.h"
 #include "System.h"
 #include "Message.h"
 #include "Task.h"
 #include "util/LinkedList.h"
 #include "util/IDs.h"

 volatile struct task_struct *sched_current_task;
 volatile ids32_t sched_task_ids;

 void sys_yield() {
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		sched_current_task->status = INACTIVE;
	}
	struct task_struct *new_task = sched_find_next_task();
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		if(new_task != sched_current_task) {
			struct task_struct *old_task = sched_current_task;
			sched_current_task = new_task;
			native_do_switch(&old_task->stack_pointer, &new_task->stack_pointer);
		}
	}
 }

 void sys_wait() {
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		sched_current_task->status = WAITING;
	}
	struct task_struct *new_task = sched_find_next_task();
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		if(new_task != sched_current_task) {
			struct task_struct *old_task = sched_current_task;
			sched_current_task = new_task;
			native_do_switch(&old_task->stack_pointer, &new_task->stack_pointer);
		}
	}
 }

 void sys_wait_timeout(uint16_t time) {

 }

 void sys_sleep(uint16_t time) {

 }

 void sys_notify(uint8_t target) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		if(target == 0) {
			return;
		}
		if(target == sched_current_task->id) {
			return;
		}
		struct task_struct *task = sched_get_task(target);
		if(task == NULL) {
			return;
		}
		if(task->status != WAITING && task->status != SLEEPING) {
			return;
		}
		task->status = INACTIVE;
	}
 }

 uint8_t sys_spawn(task_t task, size_t stack) {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		uint8_t id = ids32_aquire(&sched_task_ids);
		if(id) {
			struct task_struct *task_s = malloc(sizeof(struct task_struct));
			task_s->id = id;
			task_s->flags = 0;
			task_s->status = WAITING;
			ll_create(&task_s->messages);
			task_s->stack_head = malloc(stack);
			task_s->stack_pointer = task_s->stack_head + stack - 1;
			native_create_stack(&task_s->stack_pointer, task, &sched_task_end);
			ll_add(&sched_current_task->task_list, &task_s->task_list);
		}
		return id;
	}
	return 0;
 }

 uint8_t sys_get_current() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		return sched_current_task->id;
	}
	return 0;
 }

 void sched_init() {
	ids32_create(&sched_task_ids);
	sched_current_task = malloc(sizeof(struct task_struct));
	sched_current_task->id = ids32_aquire(&sched_task_ids);
	sched_current_task->flags = 0;
	sched_current_task->status = ACTIVE;
	ll_create(&sched_current_task->task_list);
	ll_create(&sched_current_task->messages);
	sched_current_task->stack_head = NULL;
	sched_current_task->stack_pointer = NULL;
 }

 struct task_struct *sched_get_task(uint8_t id) {
	linked_list_t *list = &sched_current_task->task_list;
	while(ll_get_elem(list, struct task_struct, task_list)->id != id) {
		list = list->next;
		if(list == &sched_current_task->task_list) {
			return NULL;
		}
	}
	return ll_get_elem(list, struct task_struct, task_list);
 }

 struct task_struct *sched_find_next_task() {
	linked_list_t *list = sched_current_task->task_list.next;
	while(ll_get_elem(list, struct task_struct, task_list)->status != INACTIVE) {
		list = list->next;
	}
	return ll_get_elem(list, struct task_struct, task_list);
 }

 void sched_task_end() {
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		sched_current_task->status = DEAD;
	}
	sched_current_task = sched_find_next_task();
	ATOMIC_BLOCK(ATOMIC_FORCEON) {
		struct task_struct *old_task = sched_current_task;
		native_dead_switch(&sched_current_task->stack_pointer);
		free(old_task->stack_head);
	}
 }