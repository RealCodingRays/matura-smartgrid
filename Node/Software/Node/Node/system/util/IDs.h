/*
 * IDs.h
 *
 * Created: 26.06.2016 11:55:23
 *  Author: Lorenzo Rai
 */ 


#ifndef IDS_H_
#define IDS_H_

#include <inttypes.h>


typedef struct ids32_struct {
	uint8_t bitmap[4];
} ids32_t;

typedef struct ids128_struct {
	uint8_t bitmap[16];
} ids128_t;


void ids32_create(ids32_t *ids);

uint8_t ids32_aquire(ids32_t *ids);

void ids32_release(ids32_t *ids, uint8_t id);


void ids128_create(ids128_t *ids);

uint8_t ids128_aquire(ids128_t *ids);

void ids128_release(ids128_t *ids, uint8_t id);

#endif /* IDS_H_ */