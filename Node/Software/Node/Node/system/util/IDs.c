/*
 * IDs.c
 *
 * Created: 26.06.2016 12:38:06
 *  Author: Lorenzo Rai
 */ 

 #include "IDs.h"

 void ids32_create(ids32_t *ids) {
	ids->bitmap[0] = 0;
	ids->bitmap[1] = 0;
	ids->bitmap[2] = 0;
	ids->bitmap[3] = 0;
 }

 uint8_t ids32_aquire(ids32_t *ids) {
	uint8_t exit = 1;
	uint8_t i = 0;
	do {
		if(ids->bitmap[i] != 0xFF) {
			exit = 0;
		} else {
			i++;
			if(i > 3) {
				return 0;
			}
		}
	} while(exit);
	exit = 1;
	uint8_t i2 = 0;
	do {
		if((ids->bitmap[i] & (1 << i2)) == 0) {
			ids->bitmap[i] |= (1 << i2);
			exit = 0;
		} else {
			i2++;
			if(i2 > 7) {
				//ERROR
				return 0;
			}
		}
	} while(exit);
	return i * 8 + i2 + 1;
 }

 void ids32_release(ids32_t *ids, uint8_t id) {
	if(id == 0 || id > 32) {
		return;
	}
	uint8_t i2 = id % 8 - 1;
	uint8_t i = (id - 1 - i2) / 8;
	ids->bitmap[i] &= ~(1 << i2);
 }


 void ids128_create(ids128_t *ids) {
	 for(uint8_t i = 0; i < 15; i++) {
		ids->bitmap[i] = 0;
	 }
 }

 uint8_t ids128_aquire(ids128_t *ids) {
	 uint8_t exit = 1;
	 uint8_t i = 0;
	 do {
		 if(ids->bitmap[i] != 0xFF) {
			 exit = 0;
			 } else {
			 i++;
			 if(i > 15) {
				 return 0;
			 }
		 }
	 } while(exit);
	 exit = 1;
	 uint8_t i2 = 0;
	 do {
		 if((ids->bitmap[i] & (1 << i2)) == 0) {
			 ids->bitmap[i] |= (1 << i2);
			 exit = 0;
			 } else {
			 i2++;
			 if(i2 > 7) {
				 //ERROR
				 return 0;
			 }
		 }
	 } while(exit);
	 return i * 8 + i2 + 1;
 }

 void ids128_release(ids128_t *ids, uint8_t id) {
	 if(id == 0 || id > 128) {
		 return;
	 }
	 uint8_t i2 = id % 8 - 1;
	 uint8_t i = (id - 1 - i2) / 8;
	 ids->bitmap[i] &= ~(1 << i2);
 }