/*
 * System.h
 *
 * Created: 25.06.2016 09:59:05
 *  Author: Lorenzo Rai
 */ 


#ifndef SYSTEM_H_
#define SYSTEM_H_

#include <stddef.h>
#include <inttypes.h>


typedef void (*task_t)();


extern void sys_yield();

extern void sys_wait();

extern void sys_wait_timeout(uint16_t time);

extern void sys_sleep(uint16_t time);

extern void sys_notify(uint8_t target);

extern uint8_t sys_spawn(task_t task, size_t stack);

extern uint8_t sys_get_current();

extern void sys_log(char *msg);

extern void sys_crash(char *msg);

#endif /* SYSTEM_H_ */