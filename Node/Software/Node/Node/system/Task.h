/*
 * Task.h
 *
 * Created: 25.06.2016 10:06:01
 *  Author: Lorenzo Rai
 */ 


#ifndef TASK_H_
#define TASK_H_

#include <inttypes.h>
#include "util/LinkedList.h"

enum task_state_enum {
	ACTIVE, INACTIVE, WAITING, SLEEPING, DEAD
};

struct task_struct {
	uint8_t id;
	linked_list_t task_list;
	linked_list_t messages;
	enum task_state_enum status;
	uint8_t flags;
	void *stack_head;
	void *stack_pointer;
};


#endif /* TASK_H_ */