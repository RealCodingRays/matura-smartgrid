/*
 * Low-Voltage Node.c
 *
 * Created: 16.05.2016 16:59:06
 * Author : Lorenzo Rai
 */ 

#include <avr/io.h>
#include "task_manager/task_manager.h"

volatile int data = 0;

void test_func() {
	data++;
}

int main(void) {
	tmgr_init();

	int id = tmgr_add_task(test_func, 256);

	tmgr_debug(id);

    /* Replace with your application code */
    while (1) {
    }
}