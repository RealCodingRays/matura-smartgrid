/*
 * task_manager.h
 *
 * Created: 16.05.2016 17:00:52
 *  Author: Lorenzo Rai
 */ 


#ifndef TASK_MANAGER_H_
#define TASK_MANAGER_H_

#include <avr/io.h>
#include <stddef.h>

void tmgr_init();

uint8_t tmgr_add_task(void (*task)(void), size_t stack_size);

void tmgr_debug(uint8_t id);

//Switches tasks and puts the current task into sleep indicating that it is waiting for a notify
void tmgr_wait();

//Switches tasks without entering sleep
void tmgr_switch();

//Frees all the resources occupied by a task
void tmgr_kill(uint8_t task);

//Releases the task from sleep and continues in the current task (No context switch)
void tmgr_notify(uint8_t task);

//Returns the id of the currently running task
uint8_t tmgr_get_current_task();

#define TMGR_RUNNING_bm 0b10000000
#define TMGR_SLEEPING_bm 0b01000000
#define TMGR_PRIORITY_bm 0b00000011

typedef struct tmgr_task {
	void *stack_pointer; // First to simplify access in assembler
	uint8_t id;
	uint8_t status;
	void *base_addr;
	void *stack_base;
	uint16_t on_time;
	struct tmgr_task *next;
	struct tmgr_task *prev;
} tmgr_task_t; 

#endif /* TASK_MANAGER_H_ */