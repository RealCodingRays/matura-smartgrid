/*
 * task_manager.c
 *
 * Created: 16.05.2016 21:18:28
 *  Author: Lorenzo Rai
 */ 

 #include <avr/io.h>
 #include <util/atomic.h>
 #include "task_manager.h"
 #include "task_manager_asm.h"

 tmgr_task_t tmgr_root_task;
 uint8_t tmgr_next_id = 0;

 void tmgr_handle_task_return() {
	printf("Task Return");
 }

 void tmgr_run_scheduler() {
	
 }

 void tmgr_set_sleep(uint8_t id, bool set) {
	tmgr_task_t *task = &tmgr_root_task;
	while(task->id != id) {
		task = task->next;
		if(task->id == 0) {
			return;
		}
	}
	if(set) {
		task->status = task->status & (~TMGR_SLEEPING_bm);
	} else {
		task->status = task->status | TMGR_SLEEPING_bm;
	}
 }

 void tmgr_init() {
	tmgr_root_task.id = 0;
	tmgr_root_task.status = TMGR_SLEEPING_bm;
	tmgr_root_task.base_addr = tmgr_run_scheduler;
	tmgr_root_task.on_time = 0;
	tmgr_root_task.stack_base = 0;
	tmgr_root_task.stack_pointer = 0;
	tmgr_root_task.prev = &tmgr_root_task;
	tmgr_root_task.next = &tmgr_root_task;
	tmgr_next_id = 1;
 }

 uint8_t tmgr_add_task(void (*task)(void), size_t stack_size) {
	uint8_t id = 0;
	if(tmgr_next_id != 0) {
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			tmgr_task_t *new_task = malloc(sizeof(tmgr_task_t));
			new_task->next = &tmgr_root_task;
			new_task->prev = tmgr_root_task.prev;
			tmgr_root_task.prev->next = new_task;
			tmgr_root_task.prev = new_task;

			new_task->id = tmgr_next_id;
			id = new_task->id;
			tmgr_next_id++;
			new_task->status = 0;
			new_task->base_addr = task;
			new_task->on_time = 0;
			new_task->stack_base = malloc(stack_size + 3 + 31); //Allocate stack, 3bytes for task return, 31bytes for status save
			new_task->stack_pointer = new_task->stack_base + stack_size - 1;
			uint8_t *stack_return = new_task->stack_pointer + 1;
			*stack_return = 0;
			*(stack_return + 1) = (uint8_t) (((uint16_t) tmgr_handle_task_return) >> 8);
			*(stack_return + 2) = (uint8_t) tmgr_handle_task_return;
		}
	}
	return id;
 }

 void tmgr_debug(uint8_t id) {
	tmgr_task_t *task = &tmgr_root_task;
	while(task->id != id) {
		task = task->next;
		if(task == &tmgr_root_task) {
			return;
		}
	}
 }

 void tmgr_wait() {
	tmgr_set_sleep(tmgr_get_current_task(), true);
 }

 void tmgr_switch() {

 }