/*
 * tak_managher_asm.h
 *
 * Created: 16.05.2016 22:55:28
 *  Author: Lorenzo Rai
 */ 


#ifndef TAK_MANAGHER_ASM_H_
#define TAK_MANAGHER_ASM_H_

extern void tmgr_switch_context(size_t stack, void *old);

#endif /* TAK_MANAGHER_ASM_H_ */